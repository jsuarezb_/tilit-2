package;

import game.screen.*;

import game.model.BoardsCache;

import game.model.save.SaveManager;

import game.utils.sound.SoundManager;

import openfl.display.Sprite;

#if kong
import game.utils.kong.KongApi;
#end

class Main extends Sprite {

    private var screenManager : ScreenManager;

    private var saveManager : SaveManager;

    private var soundManager : SoundManager;

#if kong
    private var kongApi : KongApi;
#end

	public function new () {
		super ();

        screenManager = new ScreenManager();
        screenManager.stage = stage;
        screenManager.showScreen(new MainMenuScreen());

        var fps_mem:FPS_Mem = new FPS_Mem(10, 10, 0x000000);
        addChild(fps_mem);

        saveManager = new SaveManager(stage);
        soundManager = new SoundManager(stage);

#if kong
        kongApi = new KongApi(screenManager);
#end
	}

}
