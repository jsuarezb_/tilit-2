package game;

class Path {

    public static inline var LEVELS = "assets/levels.json";

    public static inline var BACK_ICON = "graphics/reply.png";

    public static inline var NEXT_ICON = "graphics/arrow-right-bold.png";

    public static inline var USER_LEVELS_ICON = "graphics/play-circle-outline.png";

    public static inline var CLIPBOARD_ICON = "graphics/clipboard-outline.png";

    public static inline var SAVE_ICON = "graphics/content-save.png";

    public static inline var CHECK_ICON = "graphics/check.png";

    public static inline var TILE_MOVE_SOUND = "sounds/cotton-snap_short.mp3";

    public static inline var LEVEL_COMPLETE_SOUND = "sounds/ping_short.mp3";

}
