package game.utils.sound;

import openfl.display.Stage;

import openfl.net.SharedObject;

import game.model.save.SaveManager;

class SoundManager {

    private var stage : Stage;

    public function new( stage : Stage ) {
        this.stage = stage;

        stage.addEventListener(SoundEvent.ENABLE, onSoundEnable);
        stage.addEventListener(SoundEvent.DISABLE, onSoundDisable);
        stage.addEventListener(SoundEvent.TOGGLE, onSoundToggle);
        stage.addEventListener(SoundEvent.PLAY, onSoundPlay);
    }

    private function onSoundEnable( e : SoundEvent ) : Void {
        var so = SharedObject.getLocal(SaveManager.SAVE_FILE);
        so.data.audioEnabled = true;
        so.flush();
    }

    private function onSoundDisable( e : SoundEvent ) : Void {
        var so = SharedObject.getLocal(SaveManager.SAVE_FILE);
        so.data.audioEnabled = false;
        so.flush();
    }

    private function onSoundToggle( e : SoundEvent ) : Void {
        var so = SharedObject.getLocal(SaveManager.SAVE_FILE);
        so.data.audioEnabled = !so.data.audioEnabled;
        so.flush();
    }

    private function onSoundPlay( e : SoundEvent ) : Void {
        var so = SharedObject.getLocal(SaveManager.SAVE_FILE);
        if (so.data.audioEnabled == null)
            so.data.audioEnabled = true;

        if (so.data.audioEnabled && e.sound != null)
            e.sound.play();

        so.close();
    }

}
