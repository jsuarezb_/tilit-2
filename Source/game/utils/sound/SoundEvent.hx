package game.utils.sound;

import openfl.events.Event;

import openfl.media.Sound;

class SoundEvent extends Event {

    public static inline var ENABLE = "enable";

    public static inline var DISABLE = "disable";

    public static inline var TOGGLE = "toggle";

    public static inline var PLAY = "play";

    public var sound : Sound;

    public function new( label : String, sound : Sound = null,
            bubbles : Bool = false, cancelable : Bool = false) {
        super(label, bubbles, cancelable);

        this.sound = sound;
    }

}
