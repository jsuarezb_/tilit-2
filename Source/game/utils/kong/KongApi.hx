package game.utils.kong;

import game.screen.*;
import game.screen.event.ScreenEvent;

import game.model.board.Board;
import game.model.board.SerializedBoard;

import openfl.display.Stage;
import openfl.display.DisplayObject;
import openfl.display.Sprite;

import kong.Kongregate;
import kong.KongregateApi;
import kong.LoadEvent;
import kong.SaveEvent;

class KongApi {

    public static var LEVEL_CONTENT_TYPE = "Level";

    public static var LEVEL_LABEL = "user_level";

    private var screenManager : ScreenManager;

    public function new( screenManager : ScreenManager ) {
        this.screenManager = screenManager;

        Kongregate.loadApi(function (api) {
            api.sharedContent.addLoadListener('Level', onLoadLevel);
            api.services.connect();
        });
    }

    public static function browse() : Void {
        Kongregate.loadApi(function (api) {
            api.sharedContent.browse(KongApi.LEVEL_CONTENT_TYPE);
        });
    }

    public static function save( sBoard : SerializedBoard,
        thumbnail : DisplayObject, success : Void -> Void,
        error : Void -> Void ) : Void {

        var content = haxe.Json.stringify(sBoard);

        Kongregate.loadApi(function (api) {
            api.sharedContent.save(
                KongApi.LEVEL_CONTENT_TYPE,
                content,
                function (e : SaveEvent) {
                    if (e.success) success();
                    else error();
                },
                thumbnail,
                LEVEL_LABEL
            );
        });
    }

    private function onLoadLevel( e : LoadEvent ) : Void {
        var sBoard : SerializedBoard = haxe.Json.parse(e.content);
        sBoard.name = e.name;

        var screen : Screen =  new UserLevelScreen(sBoard);
        var event : ScreenEvent = new ScreenEvent(ScreenEvent.SCREEN_CHANGE, screen);
        
        screenManager.onScreenChange(event);
    }

}
