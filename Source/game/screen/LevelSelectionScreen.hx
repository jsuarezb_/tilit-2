package game.screen;

import game.model.board.Board;
import game.model.board.SerializedBoard;

import game.model.save.SaveManager;

import game.model.BoardsCache;

import game.screen.CampaignLevelScreen;
import game.screen.event.ScreenEvent;

import game.ui.buttons.Button;
import game.ui.buttons.TextButton;
import game.ui.buttons.special.BackButton;

import openfl.Assets;

import openfl.display.Bitmap;
import openfl.display.BitmapData;
import openfl.display.Sprite;

import openfl.events.MouseEvent;

import openfl.net.SharedObject;

import openfl.text.TextField;
import openfl.text.TextFieldType;
import openfl.text.TextFormat;
import openfl.text.TextFormatAlign;

#if kong
import game.utils.kong.KongApi;
#end

class LevelSelectionScreen extends Screen {

    private static inline var COLS = 10;

    public static inline var LEVEL_BUTTON_DIM = 50;

    private var levelButtons : Array<Button>;

    private var backButton : TextButton;

    private var levels : Array<SerializedBoard>;

    public function new() {
        super();

        levelButtons = new Array<Button>();
        levels = haxe.Json.parse(Assets.getText(Path.LEVELS));
    }

    override public function draw() : Void {
        drawTitle();

        var l = levels.length;
        var isAvailable : Bool, isCompleted : Bool;
        var levelIndex : TextField;

        var fmt = new TextFormat();
        fmt.font = Assets.getFont("fonts/Arcon-Regular.otf").fontName;
        fmt.size = 13;
        fmt.color = 0x333333;

        for (i in 0...l) {
            isAvailable = isLevelAvailable(i);
            isCompleted = isLevelCompleted(levels[i].id);

            var spr = BoardsCache.instance.boardIcons[i];

            var callback = (isAvailable ? function(e) changeScreen(CampaignLevelScreen.create(i)) : function(e) return);
            var button = Button.createFromSprite(spr, spr, spr, spr, callback);

            button.x = 37.5 + (i % COLS) * 75;
            button.y = 150 + Std.int(i / COLS) * 150;

            if (isCompleted) fmt.color = 0x66CC66;
            else fmt.color = 0x333333;


            levelIndex = new TextField();
            levelIndex.defaultTextFormat = fmt;
            levelIndex.selectable = false;
            levelIndex.embedFonts = true;
            levelIndex.width = LEVEL_BUTTON_DIM;
            levelIndex.height = 14;
            levelIndex.text = Std.string(i + 1);
            levelIndex.x = button.x;
            levelIndex.y = button.y + LEVEL_BUTTON_DIM;
            addChild(levelIndex);

            if (!isAvailable)
                button.alpha = .2;

            addChild(button);

            /*if (isCompleted) {
                var bmp = new Bitmap(Assets.getBitmapData(Path.CHECK_ICON));
                bmp.x = button.x + LEVEL_BUTTON_DIM - bmp.width;
                bmp.y = button.y + LEVEL_BUTTON_DIM;
                addChild(bmp);
            }*/
        }

        var fmt = new TextFormat();
        fmt.font = Assets.getFont("fonts/Arcon-Regular.otf").fontName;
        fmt.size = 20;
        fmt.color = 0x333333;

        var eLabel = new TextField();
        eLabel.defaultTextFormat = fmt;
        eLabel.embedFonts = true;
        eLabel.selectable = false;
        eLabel.width = stage.stageWidth;
        eLabel.height = 25;
        eLabel.text = "Easy";
        eLabel.x = 37.5;
        eLabel.y = 110;
        addChild(eLabel);

        var mLabel = new TextField();
        mLabel.defaultTextFormat = fmt;
        mLabel.embedFonts = true;
        mLabel.selectable = false;
        mLabel.width = stage.stageWidth;
        mLabel.height = 25;
        mLabel.text = "Normal";
        mLabel.x = 37.5;
        mLabel.y = 110 + 150;
        addChild(mLabel);

        var hLabel = new TextField();
        hLabel.defaultTextFormat = fmt;
        hLabel.embedFonts = true;
        hLabel.selectable = false;
        hLabel.width = stage.stageWidth;
        hLabel.height = 25;
        hLabel.text = "Hard";
        hLabel.x = 37.5;
        hLabel.y = 110 + 150 * 2;
        addChild(hLabel);

        backButton = BackButton.create(this, new MainMenuScreen());
        backButton.x = 25;
        backButton.y = stage.stageHeight - backButton.height - 25;
        addChild(backButton);
#if kong
        var userLevels = new Sprite();
        userLevels.addChild(new Bitmap(Assets.getBitmapData(Path.USER_LEVELS_ICON)));
        var btn = TextButton.create(userLevels, "User Levels", 150, 16, function (e) {
            KongApi.browse();
        }, 0xFF0000);
        btn.x = stage.stageWidth - btn.width - 25;
        btn.y = stage.stageHeight - btn.height - 25;
        addChild(btn);
#end
    }

    override public function destroy() : Void {
        for (btn in levelButtons)
            btn.destroy();

        backButton.destroy();
    }

    private function drawTitle() : Void {
        var fmt = new TextFormat();
        fmt.font = Assets.getFont("fonts/Arcon-Regular.otf").fontName;
        fmt.size = 30;
        fmt.align = TextFormatAlign.CENTER;

        var title = new TextField();
        title.defaultTextFormat = fmt;
        title.embedFonts = true;
        title.selectable = false;
        title.type = TextFieldType.DYNAMIC;
        title.width = stage.stageWidth;
        title.height = 32;
        title.y = 50;
        title.text = "Level Selection";
        addChild(title);
    }

    private function isLevelCompleted( id : Int ) : Bool {
        var so = SharedObject.getLocal(SaveManager.SAVE_FILE);
        var isCompleted = false;

        if (so.data.levelsCompleted != null)
            isCompleted = so.data.levelsCompleted.indexOf(id) >= 0;

        so.close();

        return isCompleted;
    }

    private function isLevelAvailable( level : Int ) : Bool {
        if (level == 0 || level == 10 || level == 20) return true;
        if (level > 0 && level < 10) return isLevelCompleted(levels[0].id);
        else if (level > 10 && level < 20) return isLevelCompleted(levels[10].id);
        else return isLevelCompleted(levels[20].id);
    }

}
