package game.screen;

import openfl.events.Event;

import game.model.BoardsCache;

import openfl.display.Sprite;

class CustomPreloader extends NMEPreloader {

    private var i = 0;

    private var percentLoaded : Float;

    public function new() {
        super();
        addEventListener(Event.ENTER_FRAME, onEnter);
    }

    private function onEnter( e : Event ) : Void {
        var b = BoardsCache.instance;
        b.loadNext();

		progress.scaleX = (percentLoaded + b.percentLoaded()) / 2;

        if (progress.scaleX >= 1)
            dispatchEvent(new Event(Event.COMPLETE));
    }

    override public function onUpdate(bL:Int, bT:Int) : Void {
        percentLoaded = bL / bT;
        if (percentLoaded > 1)
            percentLoaded = 1;
    }

    override public function onLoaded() {
        return;
    }

}
