package game.screen;

import game.screen.Screen;
import game.screen.LevelScreen;

import game.model.board.Board;
import game.model.board.SerializedBoard;

import game.ui.buttons.special.BackButton;

class LevelTestScreen extends UserLevelScreen {

    public function new( sBoard : SerializedBoard ) {
        super(sBoard);
    }

    override private function createButtons() : Void {
        var screen = LevelEditorScreen.create(sBoard);

        backButton = BackButton.create(this, screen);
        backButton.x = 25;
        backButton.y = stage.stageHeight - backButton.height - 25;

        addChild(backButton);
    }

}
