package game.screen;

import flash.text.TextField;
import flash.text.TextFieldType;
import flash.text.TextFormat;
import flash.text.TextFormatAlign;
import flash.text.AntiAliasType;

import game.ui.buttons.*;
import game.ui.buttons.special.*;

import game.ui.colorpicker.ColorPicker;

import game.ui.leveleditor.*;

import openfl.Assets;

import openfl.display.Bitmap;
import openfl.display.DisplayObject;
import openfl.display.Sprite;

import openfl.events.Event;
import openfl.events.KeyboardEvent;
import openfl.events.MouseEvent;

import game.model.board.Board;
import game.model.board.SerializedBoard;

#if kong
import game.utils.kong.KongApi;
#end

class LevelEditorScreen extends Screen {

    private static inline var SIDEBAR_X = 550;

    private static inline var SIDEBAR_Y = 10;

    private static inline var DEFAULT_BOARD_WIDTH = 5;

    private static inline var DEFAULT_BOARD_HEIGHT = 5;

    private static inline var MAX_BOARD_WIDTH = 15;

    private static inline var MAX_BOARD_HEIGHT = 10;

    private static inline var MIN_DIM = 1;

    private var boardWidth : Int;

    private var boardHeigth : Int;

    private var levelDetailsSectionTitle : TextField;

    private var levelName : String;

    private var levelUsername : TextField;

    private var levelDimensions : TextField;

    private var levelDimensionWidth : TextField;

    private var levelDimensionHeight : TextField;

    private var tileDrawButton : SelectableButton;

    private var figureDrawButton : SelectableButton;

    private var deleteButton : SelectableButton;

    private var moveButton : SelectableButton;

    private var toolsGroup : SelectableButtonGroup;

    private var tileColorsGroup : SelectableButtonGroup;

    private var colorButtons : Array<ColorTileButton>;

    private var colors : Array<Null<UInt>>;

    private var cPicker : ColorPicker;

    private var lastSidebarElement : DisplayObject;

    private var boardEditor : BoardEditor;

    private var sidebar : Sidebar;

    private var backButton : TextButton;

    private var userMessage : TextField;

    public function new( baseBoard : Board = null ) {
        super();

        colors = new Array<UInt>();
        for (i in 0...5)
            colors.push(null); // Fill colors with black

        colorButtons = new Array<ColorTileButton>();

        boardWidth = (baseBoard == null ? DEFAULT_BOARD_WIDTH : baseBoard.hCells);
        boardHeigth = (baseBoard == null ? DEFAULT_BOARD_HEIGHT : baseBoard.vCells);

        boardEditor = BoardEditor.create(boardWidth, boardHeigth, baseBoard);
    }

    public static function create( sBoard : SerializedBoard ) : Screen {
        var board = Board.getSolutionBoard(sBoard);
        var editor = new LevelEditorScreen(board);

        editor.colors = sBoard.colors;
        editor.levelName = sBoard.name;

        return editor;
    }

    override public function draw() : Void {
        sidebar = new Sidebar();
        sidebar.x = SIDEBAR_X;
        sidebar.y = SIDEBAR_Y;
        addChild(sidebar);

        addLevelDetails();

        sidebar.addSeparator();

        addTools();

        sidebar.addSeparator();

        addColors();

        sidebar.addSeparator();

        addControlButtons();

        addUIChild(boardEditor);

        addInstructions();

        stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
    }

    override public function destroy() : Void {
        levelDimensionWidth.removeEventListener(Event.CHANGE, onWidthChange);
        levelDimensionHeight.removeEventListener(Event.CHANGE, onHeightChange);
        stage.removeEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
    }

    private function addLevelDetails() : Void {
        sidebar.addSectionTitle("Dimensions");

        var normalFmt = new TextFormat();
        normalFmt.font = Assets.getFont("fonts/Arcon-Regular.otf").fontName;
        normalFmt.size = 14;

        var dim = new Sprite();
        var centerFmt = new TextFormat();
        centerFmt.font = Assets.getFont("fonts/Arcon-Regular.otf").fontName;
        centerFmt.size = 14;
        centerFmt.align = TextFormatAlign.CENTER;

        levelDimensionWidth = new TextField();
        levelDimensionWidth.defaultTextFormat = centerFmt;
        levelDimensionWidth.embedFonts = true;
        levelDimensionWidth.type = TextFieldType.INPUT;
        levelDimensionWidth.width = 30;
        levelDimensionWidth.height = 20;
        levelDimensionWidth.restrict = "0-9";
        levelDimensionWidth.maxChars = 2;
        levelDimensionWidth.text = Std.string(boardWidth);
        levelDimensionWidth.addEventListener(Event.CHANGE, onWidthChange);
        dim.addChild(levelDimensionWidth);

        var levelDimensionSeparator = new TextField();
        levelDimensionSeparator.defaultTextFormat = centerFmt;
        levelDimensionSeparator.embedFonts = true;
        levelDimensionSeparator.selectable = false;
        levelDimensionSeparator.type = TextFieldType.DYNAMIC;
        levelDimensionSeparator.width = 10;
        levelDimensionSeparator.height = 20;
        levelDimensionSeparator.text = "x";
        levelDimensionSeparator.x = levelDimensionWidth.x + 30;
        dim.addChild(levelDimensionSeparator);

        levelDimensionHeight = new TextField();
        levelDimensionHeight.defaultTextFormat = centerFmt;
        levelDimensionHeight.embedFonts = true;
        levelDimensionHeight.type = TextFieldType.INPUT;
        levelDimensionHeight.width = 30;
        levelDimensionHeight.height = 20;
        levelDimensionHeight.restrict = "0-9";
        levelDimensionHeight.maxChars = 2;
        levelDimensionHeight.text = Std.string(boardHeigth);
        levelDimensionHeight.x = levelDimensionSeparator.x + levelDimensionSeparator.width;
        levelDimensionHeight.addEventListener(Event.CHANGE, onHeightChange);
        dim.addChild(levelDimensionHeight);

        sidebar.addElement(dim);
    }

    private function addTools() : Void {
        sidebar.addSectionTitle("Tools");

        toolsGroup = new SelectableButtonGroup(Sidebar.WIDTH);
        sidebar.addElement(toolsGroup);

        var image = new Bitmap(Assets.getBitmapData("graphics/pencil.png"));
        image.width = image.height = 24;
        tileDrawButton = toolsGroup.addButton(image, function(e) {
            boardEditor.setCommand(DrawTile);
        });

        var image = new Bitmap(Assets.getBitmapData("graphics/cube-unfolded.png"));
        figureDrawButton = toolsGroup.addButton(image, function(e) {
            boardEditor.setCommand(DrawFigure);
        });

        var image = new Bitmap(Assets.getBitmapData("graphics/cursor-move.png"));
        moveButton = toolsGroup.addButton(image, function(e) {
            boardEditor.setCommand(Move);
        });

        var image = new Bitmap(Assets.getBitmapData("graphics/eraser.png"));
        image.width = image.height = 24;
        deleteButton = toolsGroup.addButton(image, function(e) {
            boardEditor.setCommand(Delete);
        });

        toolsGroup.selectButtonAtIndex(0);
    }

    private function addColors() : Void {
        sidebar.addSectionTitle("Colors");

        tileColorsGroup = new SelectableButtonGroup(Sidebar.WIDTH);
        for (i in 0...5) {
            var sprite = new ColorTileButton();

            if (colors[i] == null) { // Create color if it does not exist
                var color = Std.random(0xFFFFFF);
                colors[i] = color;
            }

            tileColorsGroup.addButton(sprite, function (e) {
                boardEditor.editorProperties.selectedColor = i;
            });


            sprite.paintBg(i, colors);
            colorButtons.push(sprite);
        }

        boardEditor.editorProperties.colors = colors;

        sidebar.addElement(tileColorsGroup);
        tileColorsGroup.selectButtonAtIndex(0);

        cPicker = new ColorPicker(function (color) {
            var i = boardEditor.editorProperties.selectedColor;
            colors[i] = color;
            paintButtonAt(i);
        });
        sidebar.addElement(cPicker);
    }

    private function paintButtonAt( i : Int ) : Void {
        var colorBtn = colorButtons[i];
        colorBtn.paintBg(i, boardEditor.editorProperties.colors);

        boardEditor.refreshFigures();
    }

    private function addInstructions() : Void {
        var fmt = new TextFormat();
        fmt.font = Assets.getFont("fonts/Arcon-Regular.otf").fontName;
        fmt.size = 14;
        fmt.align = TextFormatAlign.CENTER;

        var help = new TextField();
        help.defaultTextFormat = fmt;
        help.selectable = false;
        help.embedFonts = true;
        help.multiline = true;
        help.text = "Tile tool: T / Figure tool: F / Move: M / Delete: D / Undo: Z\nSelect colors: [1-5] / Space: Random colors";
        help.width = 550;
        help.height = help.textHeight + 10;
        help.x = 0;
        help.y = 500;
        addChild(help);
    }

    private function addControlButtons() : Void {
        var spr = new Sprite();
        spr.addChild(new Bitmap(Assets.getBitmapData(Path.CLIPBOARD_ICON)));
        sidebar.addElement(TextButton.create(
            spr, "Test", Sidebar.WIDTH, 14, function (e) {
                var sBoard = boardEditor.serializeBoard(null);
                changeScreen(new LevelTestScreen(sBoard), false);
            }
        ));

        var spr = new Sprite();
        spr.addChild(new Bitmap(Assets.getBitmapData(Path.SAVE_ICON)));
        sidebar.addElement(TextButton.create(
            spr, "Save", Sidebar.WIDTH, 14, function (e) {
#if kong
                var sBoard = boardEditor.serializeBoard(null);
                KongApi.save(sBoard, boardEditor.solutionBoard,
                    onLevelSaveSuccess, onLevelSaveError);
#end
            }
        ));

        backButton = BackButton.create(this, new MainMenuScreen());
        backButton.x = 25;
        backButton.y = stage.stageHeight - backButton.height - 25;

        userMessage = new TextField();
        userMessage.width = Sidebar.WIDTH;
        userMessage.height = 18;
        userMessage.embedFonts = true;
        userMessage.selectable = false;
        sidebar.addElement(userMessage);

        addChild(backButton);
    }

    private function onKeyDown( e : KeyboardEvent ) : Void {
        if (Std.is(stage.focus, TextField))
            return;

        switch ( e.keyCode ) {
            case 32:
                for (i in 0...5) {
                    boardEditor.editorProperties.colors[i] = Std.random(0xFFFFFF);
                    paintButtonAt(i);
                }

            case 49, 50, 51, 52, 53:
                tileColorsGroup.selectButtonAtIndex(e.keyCode - 49);

            case 84:
                toolsGroup.selectButton(tileDrawButton);

            case 70:
                toolsGroup.selectButton(figureDrawButton);

            case 77:
                toolsGroup.selectButton(moveButton);

            case 68:
                toolsGroup.selectButton(deleteButton);

            case 90:
                boardEditor.undoCommand();
        }

    }

    private function onWidthChange( e : Event) : Void {
        var w = Std.parseInt(levelDimensionWidth.text);
        if (w == null || w == 0)
            return;

        if (w > MAX_BOARD_WIDTH) {
            w = MAX_BOARD_WIDTH;
            levelDimensionWidth.text = Std.string(MAX_BOARD_WIDTH);
        }

        boardWidth = w;
        boardEditor.resizeBoard(boardWidth, boardHeigth);
    }

    private function onHeightChange( e : Event ) : Void {
        var h = Std.parseInt(levelDimensionHeight.text);
        if (h == null || h == 0)
            return;

        if (h > MAX_BOARD_HEIGHT) {
            h = MAX_BOARD_HEIGHT;
            levelDimensionHeight.text = Std.string(MAX_BOARD_HEIGHT);
        }

        boardHeigth = h;
        boardEditor.resizeBoard(boardWidth, boardHeigth);
    }

#if kong

    private function onLevelSaveSuccess() : Void {
        var fmt = new TextFormat();
        fmt.font = Assets.getFont("fonts/Arcon-Regular.otf").fontName;
        fmt.size = 14;
        fmt.align = TextFormatAlign.CENTER;
        fmt.color = 0x333333;

        userMessage.defaultTextFormat = fmt;
        userMessage.text = "Level saved!";
    }

    private function onLevelSaveError() : Void {
        var fmt = new TextFormat();
        fmt.font = Assets.getFont("fonts/Arcon-Regular.otf").fontName;
        fmt.size = 14;
        fmt.align = TextFormatAlign.CENTER;
        fmt.color = 0xFF0000;

        userMessage.defaultTextFormat = fmt;
        userMessage.text = "Connection error.";
    }

#end

}
