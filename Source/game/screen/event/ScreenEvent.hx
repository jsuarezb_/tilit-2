package game.screen.event;

import openfl.events.Event;

class ScreenEvent extends Event {

    public static inline var SCREEN_CHANGE = "screen_change";

    public var screen(default, default) : Screen;

    public function new( label : String, screen : Screen,
            bubbles : Bool = false, cancelable : Bool = false) {
        super(label, bubbles, cancelable);

        this.screen = screen;
    }

}
