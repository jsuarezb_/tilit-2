package game.screen;

import openfl.display.Sprite;

import openfl.events.Event;

import game.ui.UIObject;

import game.screen.event.ScreenEvent;

class Screen extends UIObject {

    public var screenManager : ScreenManager;

    public function new() {
        super();
    }

    public function changeScreen( newScreen : Screen, destroy : Bool = true ) : Void {
        var event = new ScreenEvent(ScreenEvent.SCREEN_CHANGE, newScreen, destroy);
        dispatchEvent(event);
    }

}
