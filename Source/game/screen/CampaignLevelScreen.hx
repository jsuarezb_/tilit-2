package game.screen;

import game.model.board.Board;

import game.model.events.LevelEvent;

import game.Path;

import game.ui.buttons.TextButton;

import openfl.Assets;

import openfl.events.KeyboardEvent;

import openfl.text.TextField;
import openfl.text.TextFieldType;
import openfl.text.TextFormat;
import openfl.text.TextFormatAlign;

import openfl.display.Sprite;
import openfl.display.Bitmap;

import game.model.BoardsCache;

import game.model.save.SaveManager;

class CampaignLevelScreen extends LevelScreen {

    private var level : Int;

    private var title : String;

    private var id : Int;

    private var isControlPressed = false;

    public function new( level : Int ) {
        super();

        this.level = level;

        var serializedBoard = Board.load(Path.LEVELS, level);
        this.id = serializedBoard.id;
        this.title = serializedBoard.name;

        board = Board.getStartBoard(serializedBoard);
        if (serializedBoard.scrambles != null)
            board.scramble(serializedBoard.scrambles);

        solutionBoard = Board.getSolutionBoard(serializedBoard);

        messages = serializedBoard.messages;
    }

    public static function create( level : Int ) : Screen {
        if (!SaveManager.mustShowCongrats()) {
            if (level < 30) {
                return new CampaignLevelScreen(level);
            } else {
                return new LevelSelectionScreen();
            }
        } else {
            return new GameCompleteScreen();
        }
    }

    override public function draw() : Void {
        super.draw();

        titleField.text = title;
    }

    private function restart() : Void {
        changeScreen(new CampaignLevelScreen(level));
    }

    override private function onLevelSolved() : Void {
        super.onLevelSolved();

        var fmt = new TextFormat();
        fmt.font = Assets.getFont("fonts/Arcon-Regular.otf").fontName;
        fmt.size = 16;
        fmt.align = TextFormatAlign.CENTER;
        fmt.color = 0x939393;

        var t = new TextField();
        t.mouseEnabled = false;
        t.defaultTextFormat = fmt;
        t.embedFonts = true;
        t.selectable = false;
        t.type = TextFieldType.DYNAMIC;
        t.text = "Press SPACE to continue";
        t.y = 550;
        t.width = stage.stageWidth;
        addChild(t);


        var backIcon = new Sprite();
        backIcon.addChild(new Bitmap(Assets.getBitmapData(Path.NEXT_ICON)));
        var nextButton = TextButton.create(backIcon, "Next", 75, 16, function (e) {
            openNextLevel();
        });
        nextButton.x = stage.stageWidth - nextButton.width - 25;
        nextButton.y = stage.stageHeight - nextButton.height - 25;
        addChild(nextButton);

        if (stage != null)
            stage.dispatchEvent(new LevelEvent(LevelEvent.COMPLETE, id));
    }

    private function openNextLevel() : Void {
        var screen = CampaignLevelScreen.create(level + 1);
        changeScreen(screen);
    }

    override private function onKeyDown( e : KeyboardEvent ) : Void {
        super.onKeyDown(e);

        switch ( e.keyCode ) {
            case 32:
                if (isSolved)
                    openNextLevel();

        }

    }

}
