package game.screen;

import game.model.board.*;

import game.ui.buttons.*;
import game.ui.buttons.special.BackButton;

import game.ui.sprites.DoneMessage;

import game.utils.sound.SoundEvent;

import openfl.Assets;

import openfl.display.Sprite;
import openfl.display.Shape;

import openfl.events.MouseEvent;
import openfl.events.KeyboardEvent;

import openfl.text.TextField;
import openfl.text.TextFormat;
import openfl.text.TextFormatAlign;

typedef LevelMessage = { text : String, keys : Array<Int> };

class LevelScreen extends Screen {

    private static inline var TOGGLE_SOLUTION_KEY = 81;

    private var board : Board;

    private var solutionBoard : Board;

    private var titleField : TextField;

    private var isSolved : Bool = false;

    private var sounds : Dynamic;

    private var messages : Array<LevelMessage>;

    private var message : TextField;

    private var currentMessage : Int = -1;

    private var backButton : TextButton;

    private var moveStack : Array<Board.Movement>;

    public function new() {
        super();

        sounds = {
            tileMove: Assets.getSound(Path.TILE_MOVE_SOUND),
            levelComplete: Assets.getSound(Path.LEVEL_COMPLETE_SOUND)
        };

        moveStack = new Array<Board.Movement>();
    }

    override public function draw() : Void {
        drawBoard();
        createTitle();
        createMessage();
        createButtons();

        stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
        stage.addEventListener(KeyboardEvent.KEY_UP, onKeyUp);
    }

    override public function destroy() : Void {
        backButton.destroy();

        stage.removeEventListener(KeyboardEvent.KEY_UP, onKeyUp);
        stage.removeEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
    }

    /** Draw the corresponding board with its solution. */
    private function drawBoard() : Void {
        addUIChild(board);
        board.refreshFigures();
        board.x = (stage.stageWidth - board.width) / 2;
        board.y = (stage.stageHeight - board.height) / 2;

        addUIChild(solutionBoard);
        solutionBoard.x = board.x;
        solutionBoard.y = board.y;
        solutionBoard.visible = false;
        solutionBoard.refreshFigures();
        solutionBoard.colorBorder(0xD99393);
    }

    private function createTitle() : Void {
        var fmt = new TextFormat();
        fmt.font = Assets.getFont("fonts/Arcon-Regular.otf").fontName;
        fmt.size = 25;
        fmt.align = TextFormatAlign.CENTER;

        titleField = new TextField();
        titleField.defaultTextFormat = fmt;
        titleField.embedFonts = true;
        titleField.selectable = false;
        titleField.y = 50;
        titleField.width = stage.stageWidth;
        addChild(titleField);
    }

    private function createMessage() : Void {
        var fmt = new TextFormat();
        fmt.font = Assets.getFont("fonts/Arcon-Regular.otf").fontName;
        fmt.size = 16;
        fmt.align = TextFormatAlign.CENTER;

        message = new TextField();
        message.defaultTextFormat = fmt;
        message.embedFonts = true;
        message.selectable = false;
        message.width = stage.stageWidth;
        message.height = 50;
        message.x = 0;
        message.y = board.y - message.height - 10;
        addChild(message);

        showNextMessage();
    }

    private function createButtons() : Void {
        backButton = BackButton.create(this, new LevelSelectionScreen());
        backButton.x = 25;
        backButton.y = stage.stageHeight - backButton.height - 25;

        addChild(backButton);
    }

    private function move( m : Board.Movement ) : Void {
        if (isSolved)
            return;

        var hasMoved = board.move(m);

        isSolved = solutionBoard.equals(board);
        if (isSolved) {
            onLevelSolved();
        } else {
            if (hasMoved && stage != null)
                stage.dispatchEvent(new SoundEvent(SoundEvent.PLAY, sounds.tileMove));
        }
    }

    private function undoMove() : Void {
        var m = moveStack.pop();
        if (m == null)
            return;

        switch ( m ) {
            case Left: move(Right);
            case Right: move(Left);
            case Down: move(Up);
            case Up: move(Down);
        }
    }

    /**
     * Function to be called when the level has been solved.
     */
    private function onLevelSolved() : Void {
        var msg = new DoneMessage(board.y + board.height);
        addUIChild(msg);

        if (stage != null)
            stage.dispatchEvent(new SoundEvent(SoundEvent.PLAY, sounds.levelComplete));

        board.colorBorder(0x93D993);
    }

    private function showNextMessage() : Void {
        if (messages == null)
            return;

        var nextIndex = currentMessage + 1;

        if (nextIndex >= messages.length) {
            message.visible = false;
            return;
        }

        currentMessage = nextIndex;
        message.text = messages[currentMessage].text;
    }

    private function toggleAudio() {
        if (stage != null)
            stage.dispatchEvent(new SoundEvent(SoundEvent.TOGGLE));
    }

    private function onKeyDown( e : KeyboardEvent ) : Void {
        switch ( e.keyCode ) {
            case 65, 37:
                move(Left);
                moveStack.push(Left);

            case 68, 39:
                move(Right);
                moveStack.push(Right);

            case 87, 38:
                move(Up);
                moveStack.push(Up);

            case 83, 40:
                move(Down);
                moveStack.push(Down);

            case 77:
                toggleAudio();

            case 90:
                undoMove();

            case TOGGLE_SOLUTION_KEY:
                solutionBoard.visible = true;
        }

        if (messages != null && currentMessage < messages.length
                && messages[currentMessage].keys.indexOf(e.keyCode) >= 0)
            showNextMessage();
    }

    private function onKeyUp( e : KeyboardEvent ) : Void {
        switch ( e.keyCode ) {
            case TOGGLE_SOLUTION_KEY:
                solutionBoard.visible = false;
        }
    }

}
