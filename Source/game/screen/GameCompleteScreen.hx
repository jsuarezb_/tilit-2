package game.screen;

import openfl.Assets;

import openfl.text.TextField;
import openfl.text.TextFieldType;
import openfl.text.TextFormat;
import openfl.text.TextFormatAlign;

import game.ui.buttons.special.BackButton;

class GameCompleteScreen extends Screen {

    public function new() {
        super();
    }

    override public function draw() : Void {
        var fmt = new TextFormat();
        fmt.font = Assets.getFont("fonts/Arcon-Regular.otf").fontName;
        fmt.size = 24;
        fmt.align = TextFormatAlign.CENTER;
        fmt.color = 0x333333;

        var t = new TextField();
        t.mouseEnabled = false;
        t.defaultTextFormat = fmt;
        t.embedFonts = true;
        t.selectable = false;
        t.type = TextFieldType.DYNAMIC;
        t.width = stage.stageWidth;
        t.height = 100;
        t.text = "Wow, you've completed the game! Congratulations!\nIf you want to play more levels, you can check out user made levels.\nThanks for playing this game!";
        t.y = (stage.stageHeight - t.height) / 2;
        t.x = 0;
        addChild(t);

        var backButton = BackButton.create(this, new MainMenuScreen());
        backButton.x = 25;
        backButton.y = stage.stageHeight - backButton.height - 25;
        addChild(backButton);
    }

}
