package game.screen;

import game.screen.Screen;
import game.screen.LevelScreen;

import game.model.board.Board;
import game.model.board.SerializedBoard;

import game.ui.buttons.special.BackButton;

class UserLevelScreen extends LevelScreen {

    private var backScreen : LevelEditorScreen;

    private var sBoard : SerializedBoard;

    public function new( sBoard : SerializedBoard ) {
        super();

        board = Board.getStartBoard(sBoard);
        if (sBoard.scrambles != null)
            board.scramble(sBoard.scrambles);

        solutionBoard = Board.getSolutionBoard(sBoard);

        this.sBoard = sBoard;
    }

    override public function draw() : Void {
        super.draw();

        titleField.text = (sBoard.name == null ? "Test Level" : sBoard.name);
    }

    override private function createButtons() : Void {
        var screen = new LevelSelectionScreen();

        backButton = BackButton.create(this, screen);
        backButton.x = 25;
        backButton.y = stage.stageHeight - backButton.height - 25;

        addChild(backButton);
    }

}
