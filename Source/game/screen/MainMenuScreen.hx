package game.screen;

import openfl.Assets;

import game.ui.buttons.TextButton;

import openfl.display.Bitmap;
import openfl.display.DisplayObject;
import openfl.display.Sprite;

import openfl.text.TextField;
import openfl.text.TextFieldType;
import openfl.text.TextFormat;
import openfl.text.TextFormatAlign;

import openfl.net.URLRequest;

class MainMenuScreen extends Screen {

    private var playButton : TextButton;

    private var editorButton : TextButton;

    private var optionsButton : TextButton;

    public function new() {
        super();
    }

    override public function draw() : Void {
        var fmt = new TextFormat();
        fmt.font = Assets.getFont("fonts/Arcon-Regular.otf").fontName;
        fmt.size = 100;
        fmt.align = TextFormatAlign.CENTER;

        var title = new TextField();
        title.defaultTextFormat = fmt;
        title.embedFonts = true;
        title.selectable = false;
        title.type = TextFieldType.DYNAMIC;
        title.width = 800;
        title.height = 100;
        title.y = 50;
        title.text = "Tilit 2";
        addChild(title);

        var spr = new Sprite();
        spr.addChild(new Bitmap(Assets.getBitmapData("graphics/play-box-outline.png")));
        playButton = TextButton.create(spr, "Play", 150, 16, function (e) {
            changeScreen(new LevelSelectionScreen());
        });
        addChild(playButton);

        spr = new Sprite();
        spr.addChild(new Bitmap(Assets.getBitmapData("graphics/tooltip-edit.png")));
        editorButton = TextButton.create(spr, "Level Editor", 150, 16, function (e) {
#if kong
            changeScreen(new LevelEditorScreen());
#else
            var url : URLRequest = new URLRequest("http://www.kongregate.com/games/resterman/tilit-2");
            openfl.Lib.getURL(url, "_blank");
#end
        });
        addChild(editorButton);

        horizontalAlignment([playButton, editorButton], 100, 400, 600);

#if !kong
        editorButton.alpha = .5;

        fmt = new TextFormat();
        fmt.font = Assets.getFont("fonts/Arcon-Regular.otf").fontName;
        fmt.size = 14;
        fmt.align = TextFormatAlign.CENTER;

        var msg = new TextField();
        msg.defaultTextFormat = fmt;
        msg.embedFonts = true;
        msg.selectable = false;
        msg.x = editorButton.x;
        msg.y = editorButton.y + editorButton.height;
        msg.width = editorButton.width;
        msg.text = "Kongregate Only!";
        addChild(msg);
#end
    }

    private function horizontalAlignment( elements : Array<DisplayObject>,
        startX : Float, startY : Float, width : Float ) : Void {

        var realWidth = width - elements[0].width;
        startX += elements[0].width / 2;
        var separation = realWidth / (elements.length - 1);

        for (i in 0...elements.length) {
            elements[i].x = startX + i * separation - elements[i].width / 2;
            elements[i].y = startY;
        }
    }

}
