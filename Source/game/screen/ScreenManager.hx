package game.screen;

import openfl.display.Stage;

import openfl.events.Event;

import game.error.ErrorMessages;

import game.screen.event.ScreenEvent;

/**
 * Class to manage screens shown in-game.
 */
class ScreenManager {

    public var stage(null, default) : Stage;

    private var currentScreen : Screen;

    public function new() {}

    /**
     * Show the specified screen, replacing the current one if present.
     *
     * @param s Screen to show.
     */
    public function showScreen( s : Screen ) : Void {
        currentScreen = s;
        currentScreen.addEventListener(ScreenEvent.SCREEN_CHANGE, onScreenChange);

        stage.addChild(currentScreen);
    }

    /**
     * Remove the current screen if present.
     */
    private function removeScreen() : Void {
        currentScreen.destroy();
        currentScreen.removeEventListener(ScreenEvent.SCREEN_CHANGE, onScreenChange);

        stage.removeChild(currentScreen);
    }

    public function onScreenChange( e : ScreenEvent ) : Void {
        if (stage == null)
            throw ErrorMessages.NO_STAGE;

        var newScreen = e.screen;

        if (newScreen == null)
            throw ErrorMessages.NULL_SCREEN;

        if (currentScreen != null)
            removeScreen();

        showScreen(newScreen);
    }

}
