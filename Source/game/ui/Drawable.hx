package game.ui;

interface Drawable {

    /**
     *Drawing function to be called whenever the object is added to stage.
     * Can't be called prior object is added to stage because {@code stage}
     * must be available.
     */
    public function draw() : Void;

}
