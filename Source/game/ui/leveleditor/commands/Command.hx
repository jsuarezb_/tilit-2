package game.ui.leveleditor.commands;

import game.model.board.DiscretePoint;

import game.ui.leveleditor.LevelEditorProperties;

enum CommandType {
    DrawTile;
    DrawFigure;
    Move;
    Delete;
}

class Command {

    private var properties : LevelEditorProperties;

    private var isMouseDown = false;

    public function new( properties : LevelEditorProperties ) {
        this.properties = properties;
    }

    public static function create( commandType : CommandType, properties : LevelEditorProperties ) : Command {
        switch ( commandType ) {
            case DrawTile:
                return new DrawTileCommand(properties);

            case DrawFigure:
                return new DrawFigureCommand(properties);

            case Move:
                return new MoveCommand(properties);

            case Delete:
                return new DeleteCommand(properties);
        }

    }

    public function mouseDown( p : DiscretePoint ) : Void {
        isMouseDown = true;
    }

    public function mouseMove( p : DiscretePoint ) : Void {}

    public function mouseUp( p : DiscretePoint ) : Void {
        isMouseDown = false;
    }

    public function undo() : Void {}

}
