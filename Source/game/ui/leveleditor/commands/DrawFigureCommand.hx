package game.ui.leveleditor.commands;

import game.model.board.Board;
import game.model.board.DiscretePoint;
import game.model.board.Figure;

import openfl.Vector;

class DrawFigureCommand extends Command {

    private var grid : Vector<Vector<Bool>>;

    private var figureCreated : Figure;

    public function new( properties : LevelEditorProperties ) {
        super(properties);

        grid = new Vector<Vector<Bool>>(properties.board.vCells);
        for (i in 0...properties.board.vCells) {
            grid[i] = new Vector<Bool>(properties.board.hCells);

            for (j in 0...properties.board.hCells)
                grid[i][j] = false;
        }
    }

    override public function mouseDown( p : DiscretePoint ) : Void {
        super.mouseDown(p);

        addTilesToFigure(p);
    }

    override public function mouseMove( p : DiscretePoint ) : Void {
        super.mouseMove(p);

        if (isMouseDown)
            addTilesToFigure(p);
    }

    override public function mouseUp( p : DiscretePoint ) : Void {
        super.mouseUp(p);
    }

    override public function undo() : Void {
        properties.board.removeFigure(figureCreated);
    }

    private function addTilesToFigure( p : DiscretePoint ) : Void {
        var board = properties.board;
        if (!board.isInsideGrid(p.x, p.y) || grid[p.y][p.x])
            return;

        if (figureCreated != null)
            board.removeFigure(figureCreated);

        grid[p.y][p.x] = true;

        var minX = board.hCells, minY = board.vCells;
        var maxX = 0, maxY = 0;

        for (y in 0...grid.length) {
            for (x in 0...grid[y].length) {
                if (grid[y][x]) {
                    minX = Std.int(Math.min(minX, x));
                    minY = Std.int(Math.min(minY, y));
                    maxX = Std.int(Math.max(maxX, x));
                    maxY = Std.int(Math.max(maxY, y));
                }
            }
        }

        var figureArray = new Array<Array<Null<UInt>>>();
        for (y in minY...(maxY+1)) {
            figureArray[y - minY] = new Array<Null<UInt>>();

            for (x in minX...(maxX+1)) {
                figureArray[y - minY][x - minX] =
                    (grid[y][x] ? properties.selectedColor : null);
            }
        }

        figureCreated = Figure.createIrregular(figureArray);
        figureCreated.drawTiles(properties.colors);
        board.addFigure(figureCreated, minX, minY);
    }

}
