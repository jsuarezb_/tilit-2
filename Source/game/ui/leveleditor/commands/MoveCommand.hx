package game.ui.leveleditor.commands;

import game.model.board.Board;
import game.model.board.DiscretePoint;
import game.model.board.Figure;

class MoveCommand extends Command {

    private var figureSelected : Figure;

    private var startPoint : DiscretePoint;

    private var prevPoint : DiscretePoint;

    public function new( properties : LevelEditorProperties ) {
        super(properties);
    }

    override public function mouseDown( p : DiscretePoint ) : Void {
        super.mouseDown(p);

        var tileFigures = properties.board.getFiguresAt(p);
        if (tileFigures.length == 0)
            return;

        figureSelected = tileFigures[tileFigures.length - 1];

        startPoint = {
            x : figureSelected.xGrid,
            y : figureSelected.yGrid
        };

        prevPoint = {
            x : p.x,
            y : p.y
        };
    }

    override public function mouseMove( p : DiscretePoint ) : Void {
        super.mouseMove(p);

        if (figureSelected == null || !isMouseDown)
            return;

        var maxX = Std.int(properties.board.hCells - figureSelected.hCells + p.x - figureSelected.xGrid);
        var maxY = Std.int(properties.board.vCells - figureSelected.vCells + p.y - figureSelected.yGrid);

        p.x = (p.x <= maxX ? p.x : maxX);
        p.y = (p.y <= maxY ? p.y : maxY);

        var figureMovement = {
            x : p.x - prevPoint.x,
            y : p.y - prevPoint.y
        };

        if (properties.board.canMoveFigure(figureSelected, figureMovement))
            properties.board.moveFigure(figureSelected, figureMovement);

        prevPoint.x = p.x;
        prevPoint.y = p.y;
    }

    override public function undo() : Void {
        if (figureSelected == null)
            return;

        var figureMovement = {
            x : startPoint.x - figureSelected.xGrid,
            y : startPoint.y - figureSelected.yGrid
        };

        properties.board.moveFigure(figureSelected, figureMovement);
    }

}
