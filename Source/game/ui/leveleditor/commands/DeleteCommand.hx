package game.ui.leveleditor.commands;

import game.model.board.Board;
import game.model.board.DiscretePoint;
import game.model.board.Figure;
import game.model.board.PlayerFigure;

class DeleteCommand extends Command {

    private var lastDeletedPosition : DiscretePoint;

    private var figuresDeleted : Array<Figure>;

    public function new( properties : LevelEditorProperties ) {
        super(properties);

        figuresDeleted = new Array<Figure>();
    }

    override public function mouseDown( p : DiscretePoint ) : Void {
        super.mouseDown(p);

        deleteFigure(p);
    }

    override public function mouseMove( p : DiscretePoint ) : Void {
        super.mouseMove(p);

        if (isMouseDown)
            deleteFigure(p);
    }

    override public function undo() : Void {
        for (figure in figuresDeleted) {
            var p = { x: figure.xGrid, y: figure.yGrid };

            if (properties.board.canInsertFigure(figure, p))
                properties.board.addFigure(figure, p.x, p.y);
        }
    }

    private function deleteFigure( p : DiscretePoint ) : Void {
        if (lastDeletedPosition != null && p.x == lastDeletedPosition.x
                && p.y == lastDeletedPosition.y)
            return;

        var tileFigures = properties.board.getFiguresAt(p);
        if (tileFigures == null || tileFigures.length == 0)
            return;

        var figure = tileFigures[tileFigures.length - 1];
        if (Std.is(figure, PlayerFigure))
            return;

        figuresDeleted.push(figure);
        properties.board.removeFigure(figure);

        lastDeletedPosition = p;
    }

}
