package game.ui.leveleditor.commands;

import game.model.board.Board;
import game.model.board.DiscretePoint;
import game.model.board.Figure;

import openfl.Vector;

class DrawTileCommand extends Command {

    private var figureSelected : Figure;

    private var grid : Vector<Vector<Bool>>;

    private var figuresAdded : Array<Figure>;

    public function new( properties : LevelEditorProperties ) {
        super(properties);

        grid = new Vector<Vector<Bool>>(properties.board.vCells);
        for (i in 0...properties.board.vCells) {
            grid[i] = new Vector<Bool>(properties.board.hCells);

            for (j in 0...properties.board.hCells)
                grid[i][j] = false;
        }

        figuresAdded = new Array<Figure>();
    }

    override public function mouseDown( p : DiscretePoint ) : Void {
        super.mouseDown(p);

        drawTile(p);
    }

    override public function mouseMove( p : DiscretePoint ) : Void {
        if (isMouseDown)
            drawTile(p);
    }

    private function drawTile( p : DiscretePoint ) : Void {
        var board = properties.board;
        if (!board.isInsideGrid(p.x, p.y) || grid[p.y][p.x])
            return;

        var figure = Figure.createIrregular([[properties.selectedColor]]);
        figure.drawTiles(properties.colors);
        board.addFigure(figure, p.x, p.y);
        grid[p.y][p.x] = true;

        figuresAdded.push(figure);
    }

    override public function undo() : Void {
        while (figuresAdded.length > 0)
            properties.board.removeFigure(figuresAdded.pop());
    }

}
