package game.ui.leveleditor;

import game.model.board.Board;

typedef LevelEditorProperties = {
    board : Board,
    selectedColor : UInt,
    colors : Array<UInt>
};
