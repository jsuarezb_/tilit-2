package game.ui.leveleditor;

import openfl.display.DisplayObject;
import openfl.display.Sprite;

import flash.text.TextField;
import flash.text.TextFieldType;
import flash.text.TextFormat;
import flash.text.AntiAliasType;

import openfl.Assets;

class Sidebar extends Sprite {

    public static inline var WIDTH = 200;

    private static inline var VERTICAL_MARGIN = 15;

    private static inline var DEFAULT_LEVEL_TITLE = "Level title";

    private var lastElementAdded : DisplayObject = null;

    public function new() {
        super();
    }

    public function addElement( element : DisplayObject ) {
        element.x = 0;
        element.y = (lastElementAdded == null
            ? 0
            : lastElementAdded.y + lastElementAdded.height + VERTICAL_MARGIN);

        lastElementAdded = element;
        addChild(element);
    }

    public function addLevelTitle( title : String = null ) : TextField {
        var titleFmt = new TextFormat();
        titleFmt.font = Assets.getFont("fonts/Arcon-Regular.otf").fontName;
        titleFmt.size = 25;

        var levelTitle = new TextField();
        levelTitle.defaultTextFormat = titleFmt;
        levelTitle.type = TextFieldType.INPUT;
        levelTitle.embedFonts = true;
        levelTitle.text = (title == null ? DEFAULT_LEVEL_TITLE : title);
        levelTitle.width = WIDTH;
        levelTitle.height = titleFmt.size + 5;
        addElement(levelTitle);

        return levelTitle;
    }

    public function addSectionTitle( text : String ) : TextField {
        var fmt = new TextFormat();
        fmt.font = Assets.getFont("fonts/Arcon-Regular.otf").fontName;
        fmt.size = 18;
        fmt.underline = true;

        var t = new TextField();
        t.defaultTextFormat = fmt;
        t.selectable = false;
        t.embedFonts = true;
        t.text = text;
        t.width = WIDTH;
        t.height = fmt.size;
        addElement(t);

        return t;
    }

    public function addSeparator() : Void {
        var separator = new Sprite();
        separator.graphics.lineStyle(1, 0x939393);
        separator.graphics.moveTo(0, 0);
        separator.graphics.lineTo(WIDTH, 0);
        addElement(separator);
    }

}
