package game.ui.leveleditor;

import openfl.display.Sprite;

import openfl.events.MouseEvent;

import game.ui.UIObject;

import game.model.board.Board;
import game.model.board.DiscretePoint;
import game.model.board.Figure;
import game.model.board.SerializedBoard;
import game.model.board.Tile;

import game.ui.leveleditor.commands.Command;

class BoardEditor extends UIObject {

    public static inline var WIDTH = 550;

    private static var DEFAULT_COMMAND = DrawTile;

    public var solutionBoard(default, set) : Board;

    public var editorProperties : LevelEditorProperties;

    public var currentCommand : Command;

    private var currentCommandType : CommandType;

    public var commandHistory : Array<Command>;

    private var isRunningCommand = false;

    public function new() {
        super();

        editorProperties = { board: null, selectedColor: 1, colors: null };
    }

    public static function create( w : Int, h : Int, baseBoard : Board = null) : BoardEditor {
        var boardEditor = new BoardEditor();

        boardEditor.solutionBoard = Board.create(w, h);
        if (baseBoard == null) {
            boardEditor.solutionBoard.insertPlayerFigure(0, 0);
        } else {
            boardEditor.solutionBoard.colors = baseBoard.colors;

            for (f in baseBoard.getFiguresWithoutPlayer())
                boardEditor.solutionBoard.addFigure(f, f.xGrid, f.yGrid);

            boardEditor.solutionBoard
                .insertPlayerFigure(baseBoard.playerFigure.xGrid,
                    baseBoard.playerFigure.yGrid);
        }

        return boardEditor;
    }

    override public function draw() : Void {
        addUIChild(solutionBoard);
        solutionBoard.x = (WIDTH - solutionBoard.width) /2;
        solutionBoard.y = (stage.stageHeight - solutionBoard.height) / 2;
        solutionBoard.colors = editorProperties.colors;

        editorProperties.board = solutionBoard;
        commandHistory = new Array<Command>();
        setCommand(DEFAULT_COMMAND);

        solutionBoard.refreshFigures();

        addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
        addEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
        stage.addEventListener(MouseEvent.MOUSE_UP, onMouseUp);
    }

    override public function destroy() : Void {
        removeEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
        removeEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
        stage.removeEventListener(MouseEvent.MOUSE_UP, onMouseUp);
    }

    private function set_solutionBoard( board : Board ) : Board {
        if (editorProperties != null)
            editorProperties.board = board;

        return this.solutionBoard = board;
    }

    /**
     * Resize the board.
     *
     * @param w New width.
     * @param h New height.
     */
    public function resizeBoard( w : Int, h : Int ) : Void {
        removeChild(solutionBoard);

        var playerPosition = {
            x: Std.int(Math.min(solutionBoard.playerFigure.xGrid, w - 1)),
            y: Std.int(Math.min(solutionBoard.playerFigure.yGrid, h - 1)),
        };

        var figures = solutionBoard.getFiguresWithoutPlayer();
        var board = Board.fill(w, h, playerPosition, figures, editorProperties.colors);
        solutionBoard = board;
        addUIChild(solutionBoard);

        solutionBoard.x = (WIDTH - solutionBoard.width) /2;
        solutionBoard.y = (stage.stageHeight - solutionBoard.height) / 2;

        setCommand(currentCommandType);
    }

    public function refreshFigures() : Void {
        solutionBoard.refreshFigures();
    }

    /**
     * Return the coordinates inside of the tile inside the grid given the
     * mouse poisition relative to the top left corner of the board.
     *
     * @param mouseX Horizontal coordinate relative to the board.
     * @param mouseY Vertical coordinate relative to the board.
     *
     * @return A point with the coordinates.
     */
    private function mouseToGrid( mouseX : Float, mouseY : Float ) : DiscretePoint {
        var x : UInt = Std.int(mouseX / Tile.DIMENSION);
        var y : UInt = Std.int(mouseY / Tile.DIMENSION);

        return {
            x : x >= solutionBoard.hCells ? solutionBoard.hCells - 1 : x,
            y : y >= solutionBoard.vCells ? solutionBoard.vCells - 1 : y
        };
    }

    public function setCommand( t : CommandType ) {
        currentCommand = Command.create(t, editorProperties);
        currentCommandType = t;
    }

    public function undoCommand() : Void {
        var lastCommand = commandHistory.pop();

        if (lastCommand != null)
            lastCommand.undo();
    }

    private function onMouseDown( e : MouseEvent ) : Void {
        var p = mouseToGrid(e.stageX - solutionBoard.x , e.stageY - solutionBoard.y);
        currentCommand.mouseDown(p);

        isRunningCommand = true;
    }

    private function onMouseMove( e : MouseEvent ) : Void {
        var p = mouseToGrid(e.stageX - solutionBoard.x , e.stageY - solutionBoard.y);
        currentCommand.mouseMove(p);
    }

    private function onMouseUp( e : MouseEvent ) : Void {
        var p = mouseToGrid(e.stageX - solutionBoard.x , e.stageY - solutionBoard.y);
        currentCommand.mouseUp(p);

        if (isRunningCommand) {
            commandHistory.push(currentCommand);
            setCommand(currentCommandType);
            isRunningCommand = false;
        }
    }

    public function serializeBoard( name : String ) : SerializedBoard {
        return solutionBoard.serialize(name, editorProperties.colors);
    }

}
