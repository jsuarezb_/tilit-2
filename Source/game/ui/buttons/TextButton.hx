package game.ui.buttons;

import openfl.Assets;

import openfl.display.DisplayObject;
import openfl.display.Sprite;

import openfl.events.MouseEvent;

import openfl.text.TextField;
import openfl.text.TextFormat;

class TextButton extends Button {

    private static inline var PADDING = 3;

    public function new( up : DisplayObject = null, over : DisplayObject = null,
        down : DisplayObject = null, hitTest : DisplayObject = null,
        callback : MouseEvent -> Void = null) {
        super(up, over, down, hitTest, callback);
    }

    public static function create( img : Sprite, label : String, width : Float,
        fontSize : Int, callback : MouseEvent -> Void,
        borderColor : UInt = 0x939393 ) : TextButton {

        var h = Math.max(fontSize, img.height);

        var sprite = new Sprite();
        sprite.graphics.beginFill(0xFFFFFF);
        sprite.graphics.lineStyle(1, borderColor, .5);
        sprite.graphics.drawRect(0, 0, width, h + 2 * PADDING);
        sprite.graphics.endFill();

        img.x = PADDING;
        img.y = PADDING;

        var fmt = new TextFormat();
        fmt.font = Assets.getFont("fonts/Arcon-Regular.otf").fontName;
        fmt.size = fontSize;

        var text = new TextField();
        text.defaultTextFormat = fmt;
        text.selectable = false;
        text.embedFonts = true;
        text.width = width - 3 * PADDING - img.width;
        text.height = h;
        text.x = img.x + img.width + PADDING;
        text.y = PADDING;
        text.text = label;

        sprite.addChild(img);
        sprite.addChild(text);

        return new TextButton(sprite, sprite, sprite, sprite, callback);
    }

}
