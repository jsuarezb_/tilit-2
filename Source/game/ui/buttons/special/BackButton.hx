package game.ui.buttons.special;

import game.Path;

import game.screen.Screen;
import game.screen.MainMenuScreen;

import game.ui.buttons.TextButton;

import openfl.display.Bitmap;
import openfl.display.Sprite;

import openfl.Assets;

class BackButton extends TextButton {

    public static function create( screenContainer : Screen, newScreen : Screen ) : TextButton {
        var backIcon = new Sprite();
        backIcon.addChild(new Bitmap(Assets.getBitmapData(Path.BACK_ICON)));

        return TextButton.create(backIcon, "Back", 75, 16, function (e) {
            screenContainer.changeScreen(newScreen);
        });
    }

}
