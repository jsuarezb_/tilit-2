package game.ui.buttons;

import openfl.display.Sprite;
import openfl.display.DisplayObject;

import openfl.events.MouseEvent;

class SelectableButton extends Sprite {

    private static inline var MARK_RADIUS = 3;

    public var isSelected(default, set) : Bool = false;

    public var hitTest : DisplayObject; // To simulate click

    private var mark : Sprite;

    private var button : Button;

    public function new( up : DisplayObject = null, over : DisplayObject = null, down : DisplayObject = null, hitTest : DisplayObject = null, callback : MouseEvent -> Void = null ) {
        super();

        this.hitTest = hitTest;

        button = new Button(up, over, down, hitTest, callback);
        addChild(button);

        mark = new Sprite();
        mark.graphics.beginFill(0xFF0000);
        mark.graphics.drawCircle(MARK_RADIUS, MARK_RADIUS, MARK_RADIUS);
        mark.graphics.endFill();
        mark.visible = false;
        addChild(mark);
    }

    public function set_isSelected( isSelected : Bool ) : Bool {
        mark.visible = isSelected;

        return this.isSelected = isSelected;
    }

    public function setCallback( callback : MouseEvent -> Void ) : Void {
        button.setCallback(callback);
    }

}
