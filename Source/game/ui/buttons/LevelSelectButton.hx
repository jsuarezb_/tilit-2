package game.ui.buttons;

import openfl.events.MouseEvent;

class LevelSelectButton extends Button {

    public static function create( callback : MouseEvent -> Void ) {
        return Button.create(
            "graphics/level_select-up.svg",
            "graphics/level_select-over.svg",
            "graphics/level_select-over.svg",
            "graphics/level_select-up.svg",
            callback
        );
    }

}
