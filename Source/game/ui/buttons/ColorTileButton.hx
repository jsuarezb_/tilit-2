package game.ui.buttons;

import game.model.board.Tile;

import openfl.display.DisplayObject;
import openfl.display.Sprite;

import openfl.events.MouseEvent;

class ColorTileButton extends Sprite {

    private var bg : Sprite = new Sprite();

    public function new() {
        super();

        graphics.beginFill(0xFFFFFF);
        graphics.drawRect(0, 0, Tile.DIMENSION, Tile.DIMENSION);
        graphics.endFill();

        addChild(bg);
    }

    /**
     * Paint the background of the button with tile's color.
     *
     * @param i Index of the color.
     * @param colors Array of colors.
     */
    public function paintBg( i : UInt, colors : Array<UInt> ) : Void {
        while (bg.numChildren > 0)
            bg.removeChildAt(0);

        var tile = Tile.create(i);
        tile.drawCenter(colors);
        bg.addChild(tile);
    }

}
