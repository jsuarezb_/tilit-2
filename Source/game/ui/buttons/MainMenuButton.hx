package game.ui.buttons;

import openfl.events.MouseEvent;

class MainMenuButton extends Button {

    public static function create( callback : MouseEvent -> Void ) {
        return Button.create(
            "graphics/main_menu-up.svg",
            "graphics/main_menu-over.svg",
            "graphics/main_menu-over.svg",
            "graphics/main_menu-up.svg",
            callback
        );
    }

}
