package game.ui.buttons;

import format.SVG;

import openfl.Assets;

import openfl.display.DisplayObject;
import openfl.display.Shape;
import openfl.display.Sprite;
import openfl.display.SimpleButton;

import openfl.events.MouseEvent;

import game.ui.Drawable;
import game.ui.Destroyable;

class Button extends SimpleButton implements Destroyable {

    private var callback : MouseEvent -> Void;

    public function new( up : DisplayObject = null, over : DisplayObject = null, down : DisplayObject = null, hitTest : DisplayObject = null, callback : MouseEvent -> Void = null) {
        super(up, over, down, hitTest);

        this.callback = callback;

        addEventListener(MouseEvent.CLICK, onClick);
    }

    public static function createSvg( up : String, over : String, down : String,
            hitTest : String, callback : MouseEvent -> Void  ) : Button {
        var svgImg : SVG;
        var upShape : Shape = null,
            overShape : Shape = null,
            downShape : Shape = null,
            hitTestShape : Shape = null;

        if (up != null) {
            svgImg = new SVG(Assets.getText(up));
            upShape = new Shape();
            svgImg.render(upShape.graphics);
        }

        if (over != null) {
            svgImg = new SVG(Assets.getText(over));
            overShape = new Shape();
            svgImg.render(overShape.graphics);
        }

        if (down != null) {
            svgImg = new SVG(Assets.getText(down));
            downShape = new Shape();
            svgImg.render(downShape.graphics);
        }

        if (hitTest != null) {
            svgImg = new SVG(Assets.getText(hitTest));
            hitTestShape = new Shape();
            svgImg.render(hitTestShape.graphics);
        }

        var button = new Button(upShape, overShape, downShape, hitTestShape, callback);

        return button;
    }

    public static function createFromSprite( up : DisplayObject,
            over : DisplayObject, down : DisplayObject, hitTest : DisplayObject,
            callback : MouseEvent -> Void ) : Button {
        return new Button(up, over, down, hitTest, callback);
    }

    public function destroy() : Void {
        removeEventListener(MouseEvent.CLICK, onClick);
    }

    private function onClick( e : MouseEvent ) : Void {
        if (callback != null)
            callback(e);
    }

    public function setCallback( callback : MouseEvent -> Void ) : Void {
        this.callback = callback;
    }

}
