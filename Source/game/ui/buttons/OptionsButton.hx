package game.ui.buttons;

import openfl.events.MouseEvent;

class OptionsButton extends Button {

    public static function create( callback : MouseEvent -> Void ) {
        return Button.create(
            "graphics/options-up.svg",
            "graphics/options-over.svg",
            "graphics/options-over.svg",
            "graphics/options-up.svg",
            callback
        );
    }

}
