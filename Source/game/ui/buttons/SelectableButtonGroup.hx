package game.ui.buttons;

import openfl.display.DisplayObject;
import openfl.display.Sprite;

import openfl.events.MouseEvent;

class SelectableButtonGroup extends Sprite {

    private var buttons : Array<SelectableButton>;

    private var maxWidth : Float;

    public function new( w : Float ) {
        super();

        maxWidth = w;
        buttons = new Array<SelectableButton>();
    }

    public function addButton( img : DisplayObject, callback : MouseEvent -> Void ) : SelectableButton {
        var button : SelectableButton;
        button = new SelectableButton(img, img, img, img, null);
        button.setCallback(function (e) {
            for (b in buttons)
                b.isSelected = false;

            button.isSelected = true;

            if (callback != null)
                callback(e);
        });

        buttons.push(button);
        arrangeButtons();
        addChild(button);

        return button;
    }

    public function getButtonAt( i : Int ) : SelectableButton {
        return buttons[i];
    }

    public function selectButton( btn : SelectableButton ) : Void {
        var i = buttons.indexOf(btn);
        selectButtonAtIndex(i);
    }

    /**
     * Select a button by index.
     *
     * @param i Index of the button inside the group.
     */
    public function selectButtonAtIndex( i : Int ) : Void {
        if (i >= buttons.length || i < 0)
            return;

        buttons[i].hitTest.dispatchEvent(new MouseEvent(MouseEvent.CLICK, true, false));
    }

    private function arrangeButtons() : Void {
        var n = maxWidth / buttons.length;

        for (i in 0...buttons.length)
            buttons[i].x = i * n;
    }

}
