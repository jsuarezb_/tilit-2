package game.ui.colorpicker;

import openfl.Assets;

import openfl.display.Bitmap;
import openfl.display.Sprite;

import openfl.events.KeyboardEvent;
import openfl.events.MouseEvent;

import game.ui.UIObject;

import flash.text.TextField;
import flash.text.TextFieldType;
import flash.text.TextFormat;
import flash.text.AntiAliasType;

import game.model.board.Tile;

class ColorPicker extends UIObject {

    private static inline var SPECTRUM_PATH = "graphics/spectrum.jpg";

    private static inline var SAMPLE_WIDTH = 60;

    private static inline var SAMPLE_HEIGHT = 20;

    private var bg : Sprite;

    private var spectrum : Sprite;

    private var spectrumBmp : Bitmap;

    private var spectrumBorder : Sprite;

    private var numberSign : TextField;

    private var colorField : TextField;

    private var colorSample : Sprite;

    private var callback : UInt -> Void;

    public function new( callback : UInt -> Void ) {
        super();

        this.callback = callback;
    }

    override public function draw() : Void {
        spectrumBmp = new Bitmap(Assets.getBitmapData(SPECTRUM_PATH));

        spectrum = new Sprite();
        spectrum.addChild(spectrumBmp);
        spectrum.x = 0;
        spectrum.y = 0;
        spectrum.alpha = Tile.ALPHA;
        addChild(spectrum);

        spectrum.addEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
        spectrum.addEventListener(MouseEvent.MOUSE_UP, onMouseUp);

        spectrumBorder = new Sprite();
        spectrumBorder.x = spectrum.x;
        spectrumBorder.y = spectrum.y;
        spectrumBorder.graphics.lineStyle(1, 0x939393);
        spectrumBorder.graphics.drawRect(0, 0, spectrum.width, spectrum.height);
        addChild(spectrumBorder);

        colorSample = new Sprite();
        colorSample.graphics.lineStyle(1, 0x939393);
        colorSample.graphics.beginFill(0x000000);
        colorSample.graphics.drawRect(0, 0, SAMPLE_WIDTH, SAMPLE_HEIGHT);
        colorSample.graphics.endFill();
        colorSample.alpha = Tile.ALPHA;
        colorSample.y = spectrum.height + 10;
        addChild(colorSample);

        var boldFmt = new TextFormat();
        boldFmt.font = Assets.getFont("fonts/Arcon-Regular.otf").fontName;
        boldFmt.size = 12;

        colorField = new TextField();
        colorField.defaultTextFormat = boldFmt;
        colorField.type = TextFieldType.INPUT;
        colorField.embedFonts = true;
        colorField.antiAliasType = AntiAliasType.ADVANCED;
        colorField.restrict = "A-F0-9";
        colorField.maxChars = 6;
        colorField.width = 80;
        colorField.height = 20;
        colorField.x = spectrum.width - colorField.width;
        colorField.y = colorSample.y;
        colorField.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
        addChild(colorField);

        numberSign = new TextField();
        numberSign.defaultTextFormat = boldFmt;
        numberSign.type = TextFieldType.DYNAMIC;
        numberSign.selectable = false;
        numberSign.embedFonts = true;
        numberSign.antiAliasType = AntiAliasType.ADVANCED;
        numberSign.text = "#";
        numberSign.width = 10;
        numberSign.height = 20;
        numberSign.x = colorField.x - numberSign.width;
        numberSign.y = colorSample.y;
        addChild(numberSign);

        paintSample(0);
        writeColorField(0);
    }

    override public function destroy() : Void {
        colorField.removeEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
        spectrum.removeEventListener(MouseEvent.MOUSE_UP, onMouseUp);
        spectrum.removeEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
    }

    private function paintSample( color : UInt ) : Void {
        colorSample.graphics.clear();
        colorSample.graphics.lineStyle(1, 0x939393);
        colorSample.graphics.beginFill(color);
        colorSample.graphics.drawRect(0, 0, SAMPLE_WIDTH, SAMPLE_HEIGHT);
        colorSample.graphics.endFill();
    }

    private function writeColorField( color : UInt ) : Void {
        colorField.text = StringTools.hex(color, 6);
    }

    private function onMouseMove( e : MouseEvent ) : Void {
        var color = spectrumBmp.bitmapData
            .getPixel(Std.int(e.localX), Std.int(e.localY));

        paintSample(color);
    }

    private function onMouseUp( e : MouseEvent ) : Void {
        var color = spectrumBmp.bitmapData
            .getPixel(Std.int(e.localX), Std.int(e.localY));

        writeColorField(color);
        callback(color);
    }

    private function onKeyDown( e : KeyboardEvent ) : Void {
        switch ( e.keyCode ) {
            case 13:
                var color = Std.parseInt("0x" + colorField.text);
                paintSample(color);
                writeColorField(color);
                callback(color);
        }
    }

}
