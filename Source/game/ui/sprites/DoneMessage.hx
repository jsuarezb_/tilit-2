package game.ui.sprites;

import game.ui.UIObject;

import openfl.Assets;

import openfl.display.Sprite;

import openfl.events.Event;

import openfl.text.TextField;
import openfl.text.TextFormat;
import openfl.text.TextFormatAlign;

class DoneMessage extends UIObject {

    private var yspeed : Float = 3.0;

    private var bg : Sprite;

    private var msg : Sprite;

    private var bottomBoard : Float;

    public function new( bottomBoard : Float ) {
        super();

        this.bottomBoard = bottomBoard;

        mouseChildren = false;
        mouseEnabled = false;
    }

    override public function draw() : Void {
        bg = new Sprite();
        bg.mouseEnabled = false;
        bg.graphics.beginFill(0xFFFFFF);
        bg.graphics.drawRect(0, 0, stage.stageWidth, stage.stageHeight);
        bg.graphics.endFill();
        addChild(bg);

        var fmt = new TextFormat();
        fmt.font = Assets.getFont("fonts/Arcon-Regular.otf").fontName;
        fmt.size = 24;
        fmt.align = TextFormatAlign.CENTER;

        var t = new TextField();
        t.defaultTextFormat = fmt;
        t.embedFonts = true;
        t.selectable = false;
        t.width = stage.stageWidth;
        t.height = 30;
        t.text = "Level complete";

        msg = new Sprite();
        msg.y = bottomBoard + 10;
        msg.addChild(t);

        addChild(msg);

        addEventListener(Event.ENTER_FRAME, onEnterFrame);
    }

    override public function destroy() : Void {
        removeEventListener(Event.ENTER_FRAME, onEnterFrame);
    }

    private function onEnterFrame( e : Event ) : Void {

        bg.alpha -= (bg.alpha > 0 ? .05 : 0);
        msg.alpha += (msg.alpha < 1 ? .1 : 0);

        msg.y += (yspeed > 0.0005 ? yspeed : 0);
        yspeed *= 0.85;
    }

}
