package game.ui;

import openfl.display.Sprite;

import openfl.events.Event;

class UIObject extends Sprite
    implements Drawable
    implements Destroyable {

    private var uiChildren : Array<UIObject>;

    private var wasAdded : Bool = false;

    public function new() {
        super();

        uiChildren = new Array<UIObject>();

        addEventListener(Event.ADDED_TO_STAGE, onAdded);
    }

    /**
     * Add a child and register it to destroy it whenever this object
     * is destroyed.
     *
     * @param child UIObject to be added.
     */
    public function addUIChild( child : UIObject ) : Void {
        uiChildren.push(child);
        addChild(child);
    }

    private function onAdded( e : Event ) : Void {
        removeEventListener(Event.ADDED_TO_STAGE, onAdded);

        draw();
    }

    /** @Override */
    public function draw() : Void { }

    public function destroy() : Void {
        if (!wasAdded)
            removeEventListener(Event.ADDED_TO_STAGE, onAdded);

        for (o in uiChildren)
            o.destroy();
    }

}
