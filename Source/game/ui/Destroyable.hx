package game.ui;

interface Destroyable {

    /**
     * Destroy listeners attached to this object and every child in it.
     */
    public function destroy() : Void;

}
