package game.error;

class ErrorMessages {

    public static inline var NULL_SCREEN = "Can't show null screen";

    public static inline var NO_STAGE = "No stage defined for screen manager";

}
