package game.model;

import game.model.board.Board;
import game.model.board.SerializedBoard;

import game.screen.LevelSelectionScreen;

import game.Path;

import openfl.Assets;

import openfl.display.Bitmap;
import openfl.display.BitmapData;

import openfl.events.Event;
import openfl.events.EventDispatcher;

import openfl.geom.Matrix;

@:file("Assets/levels.json") class LevelJson extends flash.utils.ByteArray {}

class BoardsCache extends EventDispatcher {

    public static var instance(get, null) : BoardsCache;

    public var i = 0;

    private var levels : Array<SerializedBoard>;

    public var boardIcons : Array<Bitmap>;

    public var levelIds : Array<Null<Int>>;

    public function new() {
        super();

        levels = haxe.Json.parse(new LevelJson().toString());
        levelIds = levels.map(function(b) return b.id);
        boardIcons = new Array<Bitmap>();
    }

    public function loadNext() : Void {
        if (i == levels.length)
            return;

        var bmp = getLevelBitmap(levels[i]);
        boardIcons[i] = bmp;

        i++;
    }

    public static function get_instance() : BoardsCache {
        if (instance == null)
            instance = new BoardsCache();

        return instance;
    }

    public function percentLoaded() : Float {
        return i / levels.length;
    }

    private function getLevelBitmap( serializedBoard : SerializedBoard ) : Bitmap {
        var board = Board.deserialize(
                serializedBoard.width,
                serializedBoard.height,
                serializedBoard.solutionBoard.player,
                serializedBoard.solutionBoard.figures,
                serializedBoard.colors
        );

        board.draw();
        board.refreshFigures();
        board.hideBorder();

        var scale = LevelSelectionScreen.LEVEL_BUTTON_DIM / (board.width > board.height ? board.width : board.height);
        var xt = (board.width < board.height
            ? (LevelSelectionScreen.LEVEL_BUTTON_DIM - board.width * scale) / 2 : 0);
        var yt = (board.height < board.width
            ? (LevelSelectionScreen.LEVEL_BUTTON_DIM - board.height * scale) / 2 : 0);
        var mtx = new Matrix();
        mtx.createBox(scale, scale, 0, xt, yt);

        var bmpData = new BitmapData(
            LevelSelectionScreen.LEVEL_BUTTON_DIM,
            LevelSelectionScreen.LEVEL_BUTTON_DIM,
            true,
            0x00000000
        );
        bmpData.draw(board, mtx);

        return new Bitmap(bmpData);
    }
}
