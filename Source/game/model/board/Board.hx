package game.model.board;

import openfl.display.Sprite;
import openfl.display.DisplayObject;
import openfl.display.DisplayObjectContainer;

import openfl.Assets;
import openfl.Vector;

import de.polygonal.ds.ListSet;

import game.model.board.DiscretePoint;
import game.model.board.SerializedBoard;
import game.model.board.Figure;

import game.ui.UIObject;

typedef Grid<T> = Vector<Vector<T>>;

enum Movement {
    Right;
    Up;
    Left;
    Down;
}

class Board extends UIObject {

    public var hCells(default, null) : UInt;

    public var vCells(default, null) : UInt;

    private var border : Sprite;

    private var figuresContainer : DisplayObjectContainer;

    private var tilesGrid : Grid<Array<Figure>>;

    public var figures(default, null) : Array<Figure>;

    public var playerFigure(default, null) : PlayerFigure;

    public var colors : Array<UInt>;

    public function new( w : UInt, h : UInt ) {
        super();

        figuresContainer = new Sprite();
        createEmptyBoard(w, h);
    }

    /**
     * Create a new board.
     *
     * @param w Width of the board, in tiles.
     * @param h Height of the board, in tiles.
     *
     * @return A board.
     */
    public static function create( w : UInt, h : UInt ) : Board {
        return new Board(w, h);
    }

    /**
     * Create a new board with figures and player.
     *
     * @param w Width of the board.
     * @param h Height of the board.
     * @param player Player position.
     * @param figures Figures to insert into the board.
     *
     * @return A new board.
     */
    public static function fill( w : UInt, h : UInt, player : DiscretePoint,
        figures : Array<Figure>, colors : Array<UInt>) : Board {

        var board = new Board(w, h);
        board.insertPlayerFigure(player.x, player.y);

        board.colors = colors;
        for (f in figures)
            if (board.canInsertFigure(f, {x: f.xGrid, y:f.yGrid}))
                board.addFigure(f, f.xGrid, f.yGrid);

        return board;
    }

    /**
     * Load a serialized board from levels file.
     *
     * @param path Path of levels file.
     */
    public static function load( path : String, index : Int ) : SerializedBoard {
        var levels : Array<SerializedBoard> = haxe.Json.parse(Assets.getText(path));

        if (index < 0 || index >= levels.length)
            return null;

        return levels[index];
    }

    override public function draw() : Void {
        graphics.clear();
        graphics.beginFill(0xFFFFFF);
        graphics.drawRect(0, 0, hCells * Tile.DIMENSION, vCells * Tile.DIMENSION);
        graphics.endFill();

        border = new Sprite();
        colorBorder(0x939393);

        addOrderedDisplayObjects([
            figuresContainer,
            border
        ]);
    }

    /**
     * Get every common figure, without the player figure.
     *
     * @return An array of figures.
     */
    public function getFiguresWithoutPlayer() : Array<Figure> {
        return figures.filter(function (figure) {
            return !Std.is(figure, PlayerFigure);
        });
    }

    public function refreshFigures() : Void {
        for (f in figures)
            f.drawTiles(colors);
    }

    public function colorBorder( color : UInt ) : Void {
        border.graphics.clear();
        border.graphics.lineStyle(3, color);
        border.graphics.drawRect(0, 0, hCells * Tile.DIMENSION, vCells * Tile.DIMENSION);
    }

    public function hideBorder() : Void {
        border.visible = false;
    }

    /**
     * Add display objects following a given order.
     *
     * @param orderedObjects An array of display objects to be added in order.
     */
    private function addOrderedDisplayObjects( orderedObjects : Array<DisplayObject> ) : Void {
        for (o in orderedObjects)
            addChild(o);
    }

    /**
     * Create an empty board.
     *
     * @param hCells Number of horizontal cells.
     * @param vCells Number of vertical cells.
     */
    private function createEmptyBoard( hCells : Int, vCells : Int ) : Void {
        this.hCells = hCells;
        this.vCells = vCells;

        tilesGrid = createEmptyTileGrid(hCells, vCells);
        figures = new Array();
    }

    /**
     * Create an empty tile grid.
     *
     * @param w Number of horizontal cells.
     * @param h Number of vertical cells.
     *
     * @return An empty tile grid.
     */
    private function createEmptyTileGrid( w : Int, h : Int ) : Grid<Array<Figure>> {
        var grid = new Vector<Vector<Array<Figure>>>(h);

        for (i in 0...h) {
            grid[i] = new Vector<Array<Figure>>(w);

            for (j in 0...w)
                grid[i][j] = new Array<Figure>();
        }

        return grid;
    }

    /**
     * Return the figures inside a tile.
     *
     * @param p Coordinates of the tile.
     *
     * @return The array of figures.
     */
    public function getFiguresAt( p : DiscretePoint ) : Array<Figure> {
        if (!isInsideGrid(p.x, p.y))
            return null;

        return tilesGrid[p.y][p.x];
    }

    /**
     * Add a new figure into the grid.
     *
     * @param figure Figure to add.
     * @param x Horizontal position of the top left point in the figure.
     * @param y Vertical position of the top left point in the figure.
     */
    public function addFigure( figure : Figure, x : Int, y : Int ) : Void {
        if (figure == null)
            throw "Cannot add null figure";

        figure.xGrid = x;
        figure.yGrid = y;

        addFigureInGrid(figure);

        figure.x = x * Tile.DIMENSION;
        figure.y = y * Tile.DIMENSION;

        figures.push(figure);
        figuresContainer.addChild(figure);
    }

    /**
     * Add the figure in every corresponding tile figures set into the grid.
     *
     * @param figure Figure to add.
     */
    private function addFigureInGrid( figure : Figure ) {
        var tiles = figure.tiles;

        var x : Int, y : Int;
        for (tile in tiles) {
            x = Std.int(figure.xGrid + tile.pos.x);
            y = Std.int(figure.yGrid + tile.pos.y);

            if (isInsideGrid(x,y)) {
                tilesGrid[y][x].push(figure);
            } else {
                throw "Adding " + figure.name + " out of the board";
            }
        }
    }

    /**
     * Insert the player figure into the grid.
     *
     * @param x Horizontal value of the top left point of the figure.
     * @param y Vertical value of the top left point of the figure.
     */
    public function insertPlayerFigure( x : Int, y : Int ) : Void {
        playerFigure = PlayerFigure.create();
        addFigure(playerFigure, x, y);
    }

    /**
     * Remove every reference to the figure inside the grid.
     *
     * @param figure Figure to be removed.
     */
    private function clearFigure( figure : Figure ) : Void {
        var figureX = figure.xGrid;
        var figureY = figure.yGrid;
        var tiles = figure.tiles;

        var x : Int, y : Int;
        for (tile in tiles) {
            x = Std.int(figureX + tile.pos.x);
            y = Std.int(figureY + tile.pos.y);

            tilesGrid[y][x].remove(figure);
        }
    }

    public function removeFigure( figure : Figure ) : Void {
        if (figuresContainer.contains(figure))
            figuresContainer.removeChild(figure);

        clearFigure(figure);
        figures.remove(figure);
    }

    /**
     * Check if the figure moved to p position has every tile inside the grid.
     *
     * @param figure Figure to be moved.
     * @param p Position to move the figure into.
     *
     * @return false if the figure moved to the specified position has at least
     *          one tile outside the grid, else true.
     */
    public function canInsertFigure( figure : Figure, p : DiscretePoint ) : Bool {
        var tiles = figure.tiles;

        var x : Int, y : Int;
        for (tile in tiles) {
            x = tile.pos.x + p.x;
            y = tile.pos.y + p.y;

            if (!isInsideGrid(x, y))
                return false;
        }

        return true;
    }

    /**
     * Check if a figure can be moved in a specified direction.
     *
     * @param figure The figure to check its movement.
     * @param p The movement of the figure.
     *
     * @return true if the figure stays inside the grid after the movement,
     *          else false
     */
    public function canMoveFigure( figure : Figure, p : DiscretePoint ) : Bool {
        var newFigurePosition : DiscretePoint = {
            x : p.x + figure.xGrid,
            y : p.y + figure.yGrid
        };

        return canInsertFigure(figure, newFigurePosition);
    }

    /**
     * Move a figure to a new position.
     *
     * @param figure Figure to be moved.
     * @param move Movement to apply to the figure.
     */
    public function moveFigure( figure : Figure, move : DiscretePoint ) : Void {
        clearFigure(figure);

        figure.xGrid += move.x;
        figure.yGrid += move.y;

        addFigureInGrid(figure);

        figure.x = figure.xGrid * Tile.DIMENSION;
        figure.y = figure.yGrid * Tile.DIMENSION;
    }

    /**
     * Check if the coordinates are inside the grid.
     *
     * @param x Horizontal coordinate.
     * @param y Vertical coordinate.
     *
     * @return true if the coordinates are inside the grid, else false
     */
    public inline function isInsideGrid( x : Int, y : Int ) : Bool {
        return x >= 0 && x < Std.int(hCells) && y >= 0 && y < Std.int(vCells);
    }

    /**
     * Move the player figure and every figure that needs to be moved.
     *
     * @param m Direction of movement.
     *
     * @return true if movement was done, false if not
     */
    public function move( m : Movement ) : Bool {
        var point : DiscretePoint;

        switch ( m ) {
            case Right:
                point = { x : 1, y : 0 };

            case Up:
                point = { x : 0, y : -1 };

            case Left:
                point = { x : -1, y : 0};

            case Down:
                point = { x : 0, y : 1};
        }

        // Check player is not moving outside grid
        var playerFigurePosition = {
            x : playerFigure.xGrid,
            y : playerFigure.yGrid
        };
        var newPos = {
            x : playerFigurePosition.x + point.x,
            y : playerFigurePosition.y + point.y
        };

        if (!isInsideGrid(newPos.x, newPos.y))
            return false;

        // Get figures to move
        var xPos : Int, yPos : Int;
        var figures : Array<Figure>, toMove : ListSet<Figure> = new ListSet<Figure>();
        var playerMovement = { x : point.x, y : point.y };
        var isDone = function (figures : Array<Figure>, toMove : ListSet<Figure>) {
            for (f in figures)
                if (toMove.has(f))
                    return false;

            return true;
        };

        xPos = Std.int(playerFigure.xGrid + playerMovement.x);
        yPos = Std.int(playerFigure.yGrid + playerMovement.y);

        // Add first figures to fill the toMove set with figures
        figures = tilesGrid[yPos][xPos];
        for (f in figures)
            toMove.set(f);

        while (isInsideGrid(xPos, yPos)
            && !isDone(figures = tilesGrid[yPos][xPos], toMove)) {
            // Add figures to move
            for (f in figures)
                toMove.set(f);

            playerMovement.x += point.x;
            playerMovement.y += point.y;
            xPos = playerFigure.xGrid + playerMovement.x;
            yPos = playerFigure.yGrid + playerMovement.y;
        }

        // Check if all figures that have to be moved can be moved
        var figureMove = { x : -point.x, y : -point.y };
        for (figure in toMove)
            if (!canMoveFigure(figure, figureMove))
                return false;

        // Move figures
        for (figure in toMove)
            moveFigure(figure, figureMove);

        if (toMove.size() > 0) {
            playerMovement.x -= point.x;
            playerMovement.y -= point.y;
        }
        moveFigure(playerFigure, playerMovement);

        return true;
    }

    public function scramble( times : Int ) : Void {
        if (!canMoveBoard())
            return;
            
        var ms = [Up, Left, Right, Down];

        while (times > 0)
            if (move(ms[Std.random(4)]))
                times--;
    }

    public static function deserialize( w : Int, h : Int,
            playerPosition : DiscretePoint,
            figures : Array<SerializedFigure>, colors : Array<UInt> ) : Board {

        var board = new Board(w, h);

        board.colors = colors;
        board.insertPlayerFigure(playerPosition.x, playerPosition.y);

        var figure : Figure, tiles : Array<PositionedTile>;
        for (f in figures) {
            tiles = f.tiles.map(function (tile) {
                return {
                    tile : Tile.create(tile.color),
                    pos : tile.pos
                };
            });
            figure = Figure.createIrregularPositioned(tiles);
            board.addFigure(figure, f.pos.x, f.pos.y);
        }

        return board;
    }

    /**
     * Serialize a board.
     *
     * @param name Board's name
     *
     * @return A serialized object of the board.
     */
    public function serialize( name : String, colors : Array<UInt> ) : SerializedBoard {
        var toTile : PositionedTile -> SerializedTile = function (tile) {
            return {
                pos : { x : tile.pos.x, y : tile.pos.y },
                color : tile.tile.colorIndex
            };
        };

        var toFigure : Figure -> SerializedFigure = function (figure) {
            return {
                pos : {
                    x : figure.xGrid,
                    y : figure.yGrid
                },
                width : Std.int(figure.hCells),
                height : Std.int(figure.vCells),
                tiles : figure.tiles.map(toTile)
            };
        };

        var obj : SerializedBoard = {
            id : null,
            name : name,
            width : Std.int(hCells),
            height : Std.int(vCells),
            colors : colors,
            solutionBoard: {
                player : { x : playerFigure.xGrid, y : playerFigure.yGrid },
                figures : figures.filter(function (f) {
                    return !Std.is(f, PlayerFigure);
                }).map(toFigure)
            },
            startBoard : null,
            scrambles : 1000, /* TODO FIX THIS SHIT */
            messages: null
        };

        return obj;
    }

    /**
     * Get the start board from a serialized board.
     *
     * @param sBoard A serialized board containing the data.
     *
     * @return A board based on the serialized one.
     */
    public static function getStartBoard( sBoard : SerializedBoard ) : Board {
        var board : Board;

        if (sBoard.scrambles != null) {
            board = Board.deserialize(
                sBoard.width,
                sBoard.height,
                sBoard.solutionBoard.player,
                sBoard.solutionBoard.figures,
                sBoard.colors
            );
        } else {
            board = Board.deserialize(
                sBoard.width,
                sBoard.height,
                sBoard.startBoard.player,
                sBoard.startBoard.figures,
                sBoard.colors
            );
        }

        return board;
    }

    public function canMoveBoard() : Bool {
        if (move(Up)) {
            move(Down);
            return true;
        }

        if (move(Down)) {
            move(Up);
            return true;
        }

        if (move(Right)) {
            move(Left);
            return true;
        }

        if (move(Left)) {
            move(Right);
            return true;
        }

        return false;
    }

    /**
     * Get the solution board from a serialized board.
     *
     * @param sBoard A serialized board containing the data.
     *
     * @return A board based on the serialized one.
     */
    public static function getSolutionBoard( sBoard : SerializedBoard ) : Board {
        return Board.deserialize(
            sBoard.width,
            sBoard.height,
            sBoard.solutionBoard.player,
            sBoard.solutionBoard.figures,
            sBoard.colors
        );
    }

    public function getColorGrid() : Vector<Vector<Array<UInt>>> {
        var grid = new Vector<Vector<Array<UInt>>>(vCells);
        for (i in 0...vCells) {
            grid[i] = new Vector<Array<UInt>>(hCells);

            for (j in 0...hCells)
                grid[i][j] = new Array<UInt>();
        }

        var figures = getFiguresWithoutPlayer();
        for (figure in figures)
            for (tile in figure.tiles)
                grid[figure.yGrid + tile.pos.y][figure.xGrid + tile.pos.x]
                    .push(tile.tile.colorIndex);

        return grid;
    }

    public function equals( board : Board ) : Bool {
        if (board.hCells != hCells || board.vCells != vCells)
            return false;

        var grid = getColorGrid();
        var aGrid = board.getColorGrid();

        var tile : Array<UInt>, aTile : Array<UInt>;
        var hasRemoved : Bool;
        for (y in 0...vCells) {
            for (x in 0...hCells) {
                tile = grid[y][x];
                aTile = aGrid[y][x];

                while (tile.length > 0)
                    hasRemoved = aTile.remove(tile.pop());

                if (tile.length != aTile.length)
                    return false;
            }
        }

        return playerFigure.xGrid == board.playerFigure.xGrid
            && playerFigure.yGrid == board.playerFigure.yGrid;
    }

}
