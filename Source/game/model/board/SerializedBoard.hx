package game.model.board;

import game.model.board.Figure;

import game.screen.LevelScreen;

typedef SerializedTile = {
    pos : DiscretePoint,
    color : UInt
};

typedef SerializedFigure = {
    pos : { x : Int, y : Int },
    width : Int,
    height : Int,
    tiles : Array<SerializedTile>
};

typedef SerializedBoard = {
    id : Null<Int>,
    name : String,
    width : Int,
    height : Int,
    colors : Array<UInt>,
    solutionBoard : {
        player : DiscretePoint,
        figures : Array<SerializedFigure>
    },
    startBoard : {
        player : DiscretePoint,
        figures : Array<SerializedFigure>
    },
    scrambles : Null<Int>,
    messages : Array<LevelMessage>
};
