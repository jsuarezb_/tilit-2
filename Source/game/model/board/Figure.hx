package game.model.board;

import openfl.Vector;

import openfl.display.Sprite;

import game.ui.UIObject;

typedef PositionedTile = { pos : DiscretePoint, tile : Tile };

class Figure extends UIObject {

    public var hCells : UInt;

    public var vCells : UInt;

    public var tiles : Array<PositionedTile>;

    private var tileGrid : Vector<Vector<Tile>>;

    public var xGrid : Int;

    public var yGrid : Int;

    public var isSelected : Bool = false;

    public function new( hCells : UInt, vCells : UInt ) {
        super();

        this.hCells = hCells;
        this.vCells = vCells;

        tiles = new Array<PositionedTile>();
        tileGrid = new Vector<Vector<Tile>>(vCells);
        for (i in 0...vCells) {
            tileGrid[i] = new Vector<Tile>(hCells);

            for (j in 0...hCells)
                tileGrid[i][j] = null;
        }
    }

    /**
     * Create a figure given a grid of colors.
     *
     * @param tiles Grid of colors.
     *
     * @return A figure.
     */
    public static function createIrregular( tiles : Array<Array<Null<UInt>>> ) : Figure {
        var width = Std.int(Lambda.fold(tiles, function (curr, prev) {
            return Math.max(curr.length, prev);
        }, 0));
        var height = tiles.length;

        var figure = new Figure(width, height);

        for (y in 0...tiles.length)
            for (x in 0...tiles[y].length)
                if (tiles[y][x] != null)
                    figure.addTile(Tile.create(tiles[y][x]), x, y);

        return figure;
    }

    /**
     * Create a figure given a vector of positioned tiles.
     *
     * @param tiles Vector of tile-position tuples.
     *
     * @return A figure.
     */
    public static function createIrregularPositioned( tiles : Array<PositionedTile> ) : Figure {
        var width = Lambda.fold(tiles, function (t, prev) { return Math.max(t.pos.x, prev); }, 0) + 1;
        var height = Lambda.fold(tiles, function (t, prev) { return Math.max(t.pos.y, prev); }, 0) + 1;

        var figure = new Figure(Std.int(width), Std.int(height));
        for (c in tiles)
            figure.addTile(c.tile, c.pos.x, c.pos.y);

        return figure;
    }

    /**
     * Add a tile to the figure.
     *
     * @param tile The tile to add to the figure.
     * @param x Horizontal coordinate of the tile inside the figure.
     * @param y Vertical coordinate of the tile inside the figure.
     */
    public function addTile( tile : Tile, x : Int, y : Int ) : Void {
        if (isOutside(x, y))
            throw "Cannot add Tile outside the grid";

        tileGrid[y][x] = tile;

        var positionedTile : PositionedTile = { tile : tile, pos : { x : x, y : y } };
        tiles.push(positionedTile);

        tile.x = x * Tile.DIMENSION;
        tile.y = y * Tile.DIMENSION;

        addChild(tile);
    }

    /** Draw the tiles inside the figure. */
    public function drawTiles( colors : Array<UInt> ) : Void {
        var drawnTiles = new Vector<Vector<Bool>>(vCells);

        for (i in 0...vCells) {
            drawnTiles[i] = new Vector<Bool>(hCells);

            for (j in 0...hCells) {
                drawnTiles[i][j] = false;
            }
        }

        graphics.clear(); // Always clear figure graphics
        graphics.lineStyle(2, 0x000000);

        var currentTile : Tile;
        for (y in 0...vCells)
            for (x in 0...hCells)
                drawTilesRecursive(drawnTiles, x, y, colors);

    }

    private function drawTilesRecursive( drawnTiles : Vector<Vector<Bool>>,
            x : Int, y : Int, colors : Array<UInt> ) : Void {

        var tile : Tile, top : Tile, right : Tile, bottom : Tile, left : Tile;

        if (isOutside(x, y) || drawnTiles[y][x])
            return;

        tile = getTile(x, y);
        if (tile == null)
            return;

        drawnTiles[y][x] = true;

        tile.graphics.clear(); // Clear tile graphics
        tile.drawCenter(colors);

        if (isSelected)
            graphics.moveTo(x * Tile.DIMENSION + Tile.PADDING, y * Tile.DIMENSION + Tile.PADDING);

        top = getTile(x, y - 1);
        if (top != null) {
            tile.drawTopConnection(colors);

            if (isSelected) {
                graphics.lineTo(x * Tile.DIMENSION + Tile.CONNECTION, y * Tile.DIMENSION + Tile.PADDING);
                graphics.lineTo(x * Tile.DIMENSION + Tile.CONNECTION, y * Tile.DIMENSION);
            }

            drawTilesRecursive(drawnTiles, x, y - 1, colors);

            if (isSelected) {
                graphics.moveTo(x * Tile.DIMENSION + Tile.DIMENSION - Tile.CONNECTION, y * Tile.DIMENSION);
                graphics.lineTo(x * Tile.DIMENSION + Tile.DIMENSION - Tile.CONNECTION, y * Tile.DIMENSION + Tile.PADDING);
            }
        }

        if (isSelected)
            graphics.lineTo(x * Tile.DIMENSION + Tile.DIMENSION - Tile.PADDING, y * Tile.DIMENSION + Tile.PADDING);

        right = getTile(x + 1, y);
        if (right != null) {
            tile.drawRightConnection(colors);

            if (isSelected) {
                graphics.lineTo(x * Tile.DIMENSION + Tile.DIMENSION - Tile.PADDING, y * Tile.DIMENSION + Tile.CONNECTION);
                graphics.lineTo(x * Tile.DIMENSION + Tile.DIMENSION, y * Tile.DIMENSION + Tile.CONNECTION);
            }

            drawTilesRecursive(drawnTiles, x + 1, y, colors);

            if (isSelected) {
                graphics.moveTo(x * Tile.DIMENSION + Tile.DIMENSION, y * Tile.DIMENSION + Tile.DIMENSION - Tile.CONNECTION);
                graphics.lineTo(x * Tile.DIMENSION + Tile.DIMENSION - Tile.PADDING, y * Tile.DIMENSION + Tile.DIMENSION - Tile.CONNECTION);
            }
        }

        if (isSelected)
            graphics.lineTo(x * Tile.DIMENSION + Tile.DIMENSION - Tile.PADDING, y * Tile.DIMENSION + Tile.DIMENSION - Tile.PADDING);

        bottom = getTile(x, y + 1);
        if (bottom != null) {
            tile.drawBottomConnection(colors);

            if (isSelected) {
                graphics.lineTo(x * Tile.DIMENSION + Tile.DIMENSION - Tile.CONNECTION, y * Tile.DIMENSION + Tile.DIMENSION - Tile.PADDING);
                graphics.lineTo(x * Tile.DIMENSION + Tile.DIMENSION - Tile.CONNECTION, y * Tile.DIMENSION + Tile.DIMENSION);
            }

            drawTilesRecursive(drawnTiles, x, y + 1, colors);

            if (isSelected) {
                graphics.moveTo(x * Tile.DIMENSION + Tile.CONNECTION, y * Tile.DIMENSION + Tile.DIMENSION);
                graphics.lineTo(x * Tile.DIMENSION + Tile.CONNECTION, y * Tile.DIMENSION + Tile.DIMENSION - Tile.PADDING);
            }
        }

        if (isSelected)
            graphics.lineTo(x * Tile.DIMENSION + Tile.PADDING, y * Tile.DIMENSION + Tile.DIMENSION - Tile.PADDING);

        left = getTile(x - 1, y);
        if (left != null) {
            tile.drawLeftConnection(colors);

            if (isSelected) {
                graphics.lineTo(x * Tile.DIMENSION + Tile.PADDING, y * Tile.DIMENSION + Tile.DIMENSION - Tile.CONNECTION);
                graphics.lineTo(x * Tile.DIMENSION, y * Tile.DIMENSION + Tile.DIMENSION - Tile.CONNECTION);
            }

            drawTilesRecursive(drawnTiles, x - 1, y, colors);

            if (isSelected) {
                graphics.moveTo(x * Tile.DIMENSION, y * Tile.DIMENSION + Tile.CONNECTION);
                graphics.lineTo(x * Tile.DIMENSION + Tile.PADDING, y * Tile.DIMENSION + Tile.CONNECTION);
            }
        }

        if (isSelected)
            graphics.lineTo(x * Tile.DIMENSION + Tile.PADDING, y * Tile.DIMENSION + Tile.PADDING);
    }

    /**
     * Get the tile in the specified position.
     *
     * @param x Horizontal position of the tile.
     * @param y Vertical position of the tile.
     *
     * @return A tile.
     */
    public function getTile( x : Int, y : Int ) : Tile {
        if (isOutside(x, y))
            return null;

        return tileGrid[y][x];
    }

    private function isOutside( x : Int, y : Int ) : Bool {
        return x < 0 || x >= Std.int(hCells) || y < 0 || y >= Std.int(vCells);
    }

}
