package game.model.board;

class PlayerFigure extends Figure {

    public function new() {
        super(1, 1);
    }

    public static function create() : PlayerFigure {
        var figure = new PlayerFigure();

        figure.addTile(PlayerTile.create(), 0, 0);
        figure.drawTiles(null);

        return figure;
    }

}
