package game.model.board;

import openfl.display.Sprite;

class PlayerTile extends Tile {

    private static inline var ARROW_BASE : Float = 5;

    private static inline var ARROW_HEIGHT : Float = 5;

    private static inline var ARROW_COLOR : UInt = 0x333333;

    private var topArrow : Sprite;

    private var rightArrow : Sprite;

    private var bottomArrow : Sprite;

    private var leftArrow : Sprite;

    public function new() {
        super();
    }

    public static function create() : PlayerTile {
        return new PlayerTile();
    }

    private function drawArrow() : Sprite {
        var arrow = new Sprite();

        arrow.graphics.beginFill(ARROW_COLOR);
        arrow.graphics.moveTo(-ARROW_BASE / 2, -ARROW_HEIGHT / 2);
        arrow.graphics.lineTo(ARROW_BASE / 2, -ARROW_HEIGHT / 2);
        arrow.graphics.lineTo(0, ARROW_HEIGHT / 2);
        arrow.graphics.lineTo(-ARROW_BASE / 2, -ARROW_HEIGHT / 2);
        arrow.graphics.endFill();

        return arrow;
    }

    override public function drawCenter( colors : Array<UInt> ) : Void {
        topArrow = drawArrow();
        topArrow.rotation += 180;
        topArrow.x = Tile.DIMENSION / 2;
        topArrow.y = topArrow.height / 2 + Tile.PADDING;
        addChild(topArrow);

        rightArrow = drawArrow();
        rightArrow.rotation -= 90;
        rightArrow.x = Tile.DIMENSION - rightArrow.width / 2 - Tile.PADDING;
        rightArrow.y = Tile.DIMENSION / 2;
        addChild(rightArrow);

        bottomArrow = drawArrow();
        bottomArrow.x = Tile.DIMENSION / 2;
        bottomArrow.y = Tile.DIMENSION - bottomArrow.height / 2 - Tile.PADDING;
        addChild(bottomArrow);

        leftArrow = drawArrow();
        leftArrow.rotation += 90;
        leftArrow.x = leftArrow.width / 2 + Tile.PADDING;
        leftArrow.y = Tile.DIMENSION / 2;
        addChild(leftArrow);

        this.graphics.beginFill(ARROW_COLOR);
        this.graphics.moveTo(ARROW_BASE, Tile.DIMENSION / 2);
        this.graphics.lineTo(Tile.DIMENSION / 2, ARROW_BASE);
        this.graphics.lineTo(Tile.DIMENSION - ARROW_BASE, Tile.DIMENSION / 2);
        this.graphics.lineTo(Tile.DIMENSION / 2, Tile.DIMENSION - ARROW_BASE);
        this.graphics.lineTo(ARROW_BASE, Tile.DIMENSION / 2);
        this.graphics.endFill();
    }

}
