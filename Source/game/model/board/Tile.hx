package game.model.board;

import openfl.display.Sprite;

import game.ui.UIObject;

class Tile extends UIObject {

    public static inline var BLUE_COLOR : UInt = 0x333399;

    public static inline var RED_COLOR : UInt = 0xFF3333;

    public static inline var GREEN_COLOR : UInt = 0x33FF33;

    public static inline var DIMENSION : UInt = 30;

    public static inline var PADDING : Float = 2.3;

    public static inline var CONNECTION : UInt = 7;

    public static inline var ALPHA : Float = .55;

    public var colorIndex : UInt;

    public function new() {
        super();
    }

    public static function create( colorIndex : UInt ) : Tile {
        var tile = new Tile();
        tile.colorIndex = colorIndex;

        return tile;
    }

    public function drawTopConnection( colors : Array<UInt> ) : Void {
        this.graphics.beginFill(colors[colorIndex], ALPHA);
        this.graphics.drawRect(CONNECTION, 0, DIMENSION - CONNECTION * 2, PADDING);
        this.graphics.endFill();
    }

    public function drawRightConnection( colors : Array<UInt> ) : Void {
        this.graphics.beginFill(colors[colorIndex], ALPHA);
        this.graphics.drawRect(DIMENSION - PADDING, CONNECTION, PADDING, DIMENSION - CONNECTION * 2);
        this.graphics.endFill();
    }

    public function drawBottomConnection( colors : Array<UInt> ) : Void {
        this.graphics.beginFill(colors[colorIndex], ALPHA);
        this.graphics.drawRect(CONNECTION, DIMENSION - PADDING, DIMENSION - CONNECTION * 2, PADDING);
        this.graphics.endFill();
    }

    public function drawLeftConnection( colors : Array<UInt> ) : Void {
        this.graphics.beginFill(colors[colorIndex], ALPHA);
        this.graphics.drawRect(0, CONNECTION, PADDING, DIMENSION - CONNECTION * 2);
        this.graphics.endFill();
    }

    public function drawCenter( colors : Array<UInt> ) : Void {
        this.graphics.beginFill(colors[colorIndex], ALPHA);
        this.graphics.drawRect(PADDING, PADDING, DIMENSION - PADDING * 2, DIMENSION - PADDING * 2);
        this.graphics.endFill();
    }

}
