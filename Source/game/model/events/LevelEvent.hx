package game.model.events;

import openfl.events.Event;

class LevelEvent extends Event {

    public static var COMPLETE = "complete";

    public var id : Int;

    public function new( label : String, id : Int,
            bubbles : Bool = false, cancelable : Bool = false) {
        super(label, bubbles, cancelable);

        this.id = id;
    }

}
