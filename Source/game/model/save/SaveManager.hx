package game.model.save;

import openfl.display.Stage;

import openfl.net.SharedObject;

import game.model.events.LevelEvent;

#if kong
import kong.Kongregate;
import kong.KongregateApi;
#end

class SaveManager {

    public static var SAVE_FILE = "tilit2";

    public static var LEVELS_COMPLETED_VALUE = "levelsCompleted";

    private var stage : Stage;

    public function new( stage : Stage ) {
        this.stage = stage;

        stage.addEventListener(LevelEvent.COMPLETE, onLevelComplete);
    }

    private function onLevelComplete( e : LevelEvent ) : Void {
        var i = e.id;

        var so = SharedObject.getLocal(SAVE_FILE);
        if (so.data.levelsCompleted == null)
            so.data.levelsCompleted = new Array<UInt>();

        if (so.data.levelsCompleted.indexOf(i) == -1)
            so.data.levelsCompleted.push(i);

        so.flush();

#if kong
        Kongregate.loadApi(function ( api : KongregateApi ) {
            var l = so.data.levelsCompleted.length;
            api.stats.submit("Levels completed", l);
        });
#end
    }

    public static function mustShowCongrats() : Bool {
        var so = SharedObject.getLocal(SAVE_FILE);

        if (so.data.levelsCompleted == null)
            return false;

        var completed = true;
        for (i in BoardsCache.instance.levelIds)
            completed = (so.data.levelsCompleted.indexOf(i) > -1) && completed;

        if (completed && so.data.hasCompleted == null) {
            so.data.hasCompleted = true;
            so.flush();
            return true;
        } else {
            return false;
        }
    }

}
